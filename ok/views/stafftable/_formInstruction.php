<?php
use common\components\AnguHtml;
use common\modules\roles\models\ACLRole;
use common\modules\ok\Module;

if (!isset($itemName)) {
    $itemName = 'position';
    $prefix = '-{{position.ID}}';    
} else $prefix = ''; 
$prefixName = str_replace('-', '_', $prefix);
?>
       <form name="form<?= $prefixName ?>">     
       <div class="row">
           <div class="col-xs-8">
           <?= AnguHtml::autoCompleteField($baseModelsNamespace.'StaffPosition', 'StaffInstructionID', [
                   'id' => "position{$prefix}-instructions",
                   'selected-object' => 'selectInstruction',
                   'initial-value' => "{{{$itemName}.Instruction}}",
                   'div' => ['ng-hide' => "{$itemName}.addinstruction", 'ng-init' => "{$itemName}.addinstruction = true"]
           ], $this) ?>  
           <div ng-show="<?= $itemName ?>.addinstruction">
               <?= AnguHtml::textInput($baseModelsNamespace.'StaffInstruction', 'Number', ['ng-model' => "{$itemName}.changed.Instruction.Number"])?>
               <?= AnguHtml::dateInput($baseModelsNamespace.'StaffInstruction', 'Date', ['ng-model' => "{$itemName}.changed.Instruction.Date"])?>
               <?= AnguHtml::textarea($baseModelsNamespace.'StaffInstruction', 'Text', ['ng-model' => "{$itemName}.changed.Instruction.Text", 'div' => ['style' => "display: none"]])?>
           </div>    
           </div>
           <div class="col-xs-4" style="padding-top: 25px; display: none;" ng-show="false">
           <?= AnguHtml::aButton('{{'.$itemName.'.addinstruction ? "' . Module::t('ML', 'Hide') . '" : "' . Module::t('ML', 'Add') . '"}}',
                   ['ng-click' => "addInstruction({$itemName})",'btnType' => 'default'])?>               
           </div>
        </div>
          
       <div style="clear:left; float:right">
           
           <?= AnguHtml::aButton(Module::t('ML', isset($isNewRecord) && $isNewRecord ? 'Create' : 'Update'),
                ['btnType' => 'primary',                    
                 'ng-click' => "updatePositionItems({$itemName},\$event,1)",                
                 'ng-disabled' => "form".$prefixName.".\$invalid",
                 ]) ?>
       </div>      
       </form>    
