<?php

use yii\helpers\Html;
use common\modules\roles\models\ACLRole;
use common\modules\ok\assets\StaffTableAsset;
use common\components\AnguHtml;
use yii\bootstrap\ButtonDropdown;
use common\modules\ok\Module;

/* @var $this yii\web\View */

$this->title = Module::t('ML', 'Staff Table');
$this->params['breadcrumbs'][] = $this->title;

StaffTableAsset::register($this);

$baseModelsNamespace = str_replace('controllers', 'models', $this->context->module->controllerNamespace).'\\';
?>

<div class="staff-table-index">
    <h1><?= Html::encode($this->title) ?></h1>
    <div ng-modules="stafftable" ng-controller="stafftableController" ng-cloak="">
        <?= AnguHtml::a("{{globalMode ? 'Показать' : 'Скрыть'}} структуру",['ng-click' => "globalMode = !globalMode;", 'style' => "float:left;"]) ?>
        <br/>        
        <?= AnguHtml::errorBlock()?>
        <br/>
        <div class="col-md-4" ng-hide="globalMode">
        <?= AnguHtml::autoCompleteField($baseModelsNamespace.'StaffPosition', 'DepartmentParentID', [
              'id' => "searchDepartment",
              'placeholder' => Module::t('ML', 'Search').' '.Module::t('ML', 'Department')], $this, 'stafftable', 'ok', 'suggesttree') ?>
        <?= AnguHtml::autoCompleteField($baseModelsNamespace.'Employee', 'PersonID', [
              'id' => "searchPerson",
              'placeholder' => Module::t('ML', 'Search').' '.Module::t('ML', 'Person')], $this, 'stafftable', 'ok', 'suggestperson') ?> 
        <?= AnguHtml::yaTreeView([], ['ng-class' => "{selnode: context.selectedNodes.indexOf(node) > -1}"], $this) ?>
        </div>
        <div class="col-md-{{globalMode ? '12': '8'}} ng-hide" ng-show="context.selectedNodes.length > 0">
            <h3>{{context.selectedNodes[0].$model.label}}</h3>
            <?= AnguHtml::tagInit(['categories' => $categories, 
                'financeTypes' => $financeTypes, 
                'increaseTypes' => $increaseTypes, 
                'rates' => $rates,
                'decisionTypes' => $decisionTypes]) ?>
            
            <p></p>
                <?=
                ACLRole::accessControllerAction('create') ? AnguHtml::aButton('{{newposition.Selected ? "' . Module::t('ML', 'Hide') . '" : "' .
                                Module::t('ML', 'Create {modelClass}', ['modelClass' => Module::t('ML', 'Staff Position RP')]) . '"}}'
                                , ['ng-click' => 'addPosition(newposition)','btnType' => 'success']) : ''
                ?>
            
            <?php if($reports = common\modules\reports\models\Report::reportForm('StaffTable')): ?>
                <?=
                    ButtonDropdown::widget([
                        'label' => Module::t('ML', 'Reports'),
                        'dropdown' => [
                            'items' => $reports,
                        ],
                        'options' => [
                            'class' => 'btn btn-success'
                        ]
                    ])
                ?>
            <?php endif; ?>
            
                <?=
                    ACLRole::accessControllerAction('index','staffgeneralorder','orders') ? 
                        Html::a(Module::t('ML', 'Show orders by department'), '/orders/staffgeneralorder/index?DepartmentParentID={{departmentParentID}}', ['class' => 'btn btn-default','target'=>'_blank']) 
                    : ''
                ?>
            
            <?=
                AnguHtml::aButton('{{showschema ? "' . Module::t('ML', 'Hide') . '" : "' .
                                Module::t('ML', 'Show Schema') . '"}}'
                                , ['ng-click' => 'showschema = !showschema','btnType' => 'default'])
            ?>
            <?= (ACLRole::accessControllerAction('create','employeeorder')) ? 
                AnguHtml::aButton('{{ProtocolMode ? "' . Module::t('ML', 'Hide Protocol') . '" : "' .
                                Module::t('ML', 'Show Protocol') . '"}}'
                                , ['ng-click' => 'showAllProtocols()','btnType' => 'default']) : ''
            ?>
            
            <div ng-show="showschema" class="lgnd">
                <div class="lgnd"><div class="square typelike"></div> <div>Ставка добавляется</div></div>                
                <div class="lgnd"><div class="square typerawadd"></div> <div>Ставка изменяется</div></div>
                <div class="lgnd"><div class="square typedel"></div> <div>Ставка удаляется</div></div>
                <div class="lgnd"><div class="lgndtext fontsel">ФИО</div> <div>Сотрудник добавляется</div></div>
                <div class="lgnd"><div class="lgndtext fontrawadd">ФИО</div> <div>Сотрудник изменяется</div></div>
                <div class="lgnd"><div class="lgndtext fontdel">ФИО</div> <div>Сотрудник удаляется</div></div>    
                <div class="lgnd"><div class="lgndtext fontabsent">ФИО</div> <div>Сотрудник временно отсутствует</div></div>
                <div class="lgnd"><div class="lgndtext fontparttime">ФИО</div> <div>Сотрудник работает во время отпуска</div></div>
                <div class="lgnd" ng-show="false"><div class="lgndtext"><span class="glyphicon glyphicon-question-sign"></span></div> <div>Инструкция</div></div>
            </div>
            <p></p>
            <table class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th/>    
                        <th><?= Module::t('ML', 'Post') ?></th>
                        <th><?= Module::t('ML', 'Rate') ?></th>
                        <th><?= Module::t('ML', 'Category') ?></th>
                        <th><?= Module::t('ML', 'Finance Type') ?></th>
                        <th><?= Module::t('ML', 'Value') ?></th>
                        <th><?= Module::t('ML', 'Hold') ?></th>                
                        <th><?= Module::t('ML', 'Free') ?></th>
                        <th><?= Module::t('ML', 'Person') ?></th>
                        <th><?= Module::t('ML', 'Instruction') ?></th>
                        <th/>
                    </tr>
                    <tr class="filters form-group-littleskip">
                        <td/>    
                        <td><?=AnguHtml::dropDownList($baseModelsNamespace.'Post', 'PostID', [
                       'ng' => [
                           'model' => "flt.PostID",
                           'repeat' => "[{ID : 0, Name : ''}].concat(posts)",], 'labelName' => ''])?></td>
                        <td></td>
                        <td><?= AnguHtml::dropDownList($baseModelsNamespace.'StaffPosition', 'StaffCategoryID', [
                       'ng' => [
                           'model' => "flt.StaffCategoryID",
                           'repeat' => "[{ID : 0, Name : ''}].concat(categories)",], 'labelName' => ''])?></td>
                        <td><?= AnguHtml::dropDownList($baseModelsNamespace.'StaffPosition', 'FinanceTypeID', [
                       'ng' => [
                           'model' => "flt.FinanceTypeID",
                           'repeat' => "[{ID : 0, Name : ''}].concat(financeTypes)",], 'labelName' => ''])?></td>
                        <td></td>
                        <td></td>                
                        <td></td>
                        <td></td>
                        <td></td>
                        <td/>
                    </tr>
                </thead>                
                <tbody>
                    <tr ng-if="newposition.Selected">
                        <td/>
                        <td colspan="10">
                            <?= $this->render('_form',['baseModelsNamespace' => $baseModelsNamespace,'isNewRecord' => true,'itemName' => 'newposition']);?>
                        </td>
                    </tr>
                    <tr ng-repeat-start="position in resultArray = (positions | orderBy : ['Post', 'ParentID', 'ID'] | filter : {PostID : flt.PostID > 0 ? flt.PostID : undefined, 
                                                                                                                   StaffCategoryID : flt.StaffCategoryID > 0 ? flt.StaffCategoryID : undefined, 
                                                                                                                   FinanceTypeID : flt.FinanceTypeID > 0 ? flt.FinanceTypeID : undefined})" 
                        ng-init="position.Selected = position.Selected > 0; position.InstructionMode = false;" ng-class="{{classForTrTd}}" ng-if="position.ID > 0"/>  
                        <td style="background-color: #fff" rowspan="{{position.InProcess == 0 ? position.Persons.length+1+(position.Selected ? 2 : 0) : position.Persons.length+4+(position.Selected ? 2 : 0)}}"
                            ng-if="position.ActualParentID == 0"><?= ACLRole::checkAccess('admin') ? '{{position.ParentID}}' : ''?></td>
                        <td ng-show="position.ProtocolMode" ng-class="{{classForTrTd}}" rowspan="{{position.Persons.length+1}}" colspan="7">
                            <?= $this->render('_formProtocol', ['baseModelsNamespace' => $baseModelsNamespace]);?>
                        </td>
                        <td ng-hide="position.ProtocolMode" ng-class="{{classForTrTd}}" rowspan="{{position.Persons.length+1}}" ng-mouseover="setInfo(position)" ng-mouseout="setInfo(position)">{{position.Post}}{{position.AlterPostName.length>0 ? ' ('+position.AlterPostName+')' : ''}} <br/><br/><i>{{position.Info ? position.Text : ''}}</i></td>
                        <td ng-hide="position.ProtocolMode" ng-class="{{classForTrTd}}" rowspan="{{position.Persons.length+1}}">{{position.Rate}}</td>
                        <td ng-hide="position.ProtocolMode" ng-class="{{classForTrTd}}" rowspan="{{position.Persons.length+1}}">{{position.StaffCategory}}</td>
                        <td ng-hide="position.ProtocolMode" ng-class="{{classForTrTd}}" rowspan="{{position.Persons.length+1}}">{{position.FinanceType}}</td>
                        <td ng-hide="position.ProtocolMode" ng-class="{{classForTrTd}}" rowspan="{{position.Persons.length+1}}">{{position.Value}}</td>
                        <td ng-hide="position.ProtocolMode" ng-class="{{classForTrTd}}" rowspan="{{position.Persons.length+1}}">{{position.HoldWOAbsent > 0 ? position.HoldWOAbsent : 0}}{{position.HoldWithAbsent - position.HoldWOAbsent > 0 ? ' ('+position.HoldWithAbsent+')' : ''}}</td>                   
                        <td ng-hide="position.ProtocolMode" ng-class="{{classForTrTd}}" rowspan="{{position.Persons.length+1}}">{{(position.Value - position.HoldWOAbsent).toFixed(2)}}{{position.HoldWithAbsent - position.HoldWOAbsent > 0 ? ' ('+(position.Value - position.HoldWithAbsent).toFixed(2)+')' : ''}}</td>
                        <td ng-class="{fontsel : position.Persons[0].Person!== undefined && position.Persons[0].EmployeeRaw == 1,
                                       fontrawadd : position.Persons[0].Person!== undefined && position.Persons[0].EmpInProcess > 0,
                                       fontdel: position.Persons[0].Person!== undefined && position.Persons[0].EmpExit > 0,
                                       fontabsent: position.Persons[0].Person!== undefined && position.Persons[0].EmpInProcess == 0 && position.Persons[0].EmployeeStateID == 100391,
                                       fontparttime: position.Persons[0].Person!== undefined && position.Persons[0].EmpInProcess == 0 && position.Persons[0].EmployeeStateID == 100392}" 
                                       ng-class="{{classForTrTd}}">{{position.Persons[0].Person}} {{position.Persons[0].PersonValue}} {{position.Persons[0].BeforeVacationValue > 0 ? '('+position.Persons[0].BeforeVacationValue+')' : ''}}
                           <div ng-show="ProtocolMode"><p><i>{{protocolInfo(position.Persons[0].EmployeeParentID)}}</i></p></div>
                           <?php if(ACLRole::accessControllerAction('create','employeeorder')):?>
                           <?= AnguHtml::spanButton("glyphicon glyphicon-file", "/ok/employeeorder/create?employeeParentID={{position.Persons[0].EmployeeParentID}}",
                                ['title' => Module::t('ML', 'Create {modelClass}', [
				'modelClass' => Module::t('ML', 'Employee Order')]), 
                                'target' => "_blank", 
                                'ng-show' => "position.Persons[0].Person!== undefined && position.Persons[0].EmployeeRaw == 0 && position.Persons[0].EmpInProcess == 0"], 'create', 'employeeorder') ?> 
                            <?php endif;?>
                           <?php if(ACLRole::accessControllerAction('index','employee')):?>
                           <?= AnguHtml::spanButton("glyphicon glyphicon-eye-open", "/ok/employee/index?parentid={{position.Persons[0].EmployeeParentID}}",
                                ['title' => Module::t('ML', 'View'), 
                                'target' => "_blank", 
                                'ng-show' => "position.Persons[0].Person!== undefined"], 'index', 'employee') ?> 
                            <?php endif;?>
                            <?php if(ACLRole::accessControllerAction('create','employeeorder')):?>
                            <?= AnguHtml::spanButton("glyphicon glyphicon-list", null,
                                ['title' => Module::t('ML', 'Protocol'),
                                 'ng-click' => "showProtocol(position.Persons[0],position)",
                                 'class' => 'pointer',
                                 'ng-show' => "position.Persons[0].Person!== undefined && position.Persons[0].EmployeeRaw == 0 && position.Persons[0].EmpInProcess == 0"   
                                    ], 'index', 'employee') ?> 
                            <?php endif;?>
                        </td>
                        <td ng-class="{{classForTrTd}}" rowspan="{{position.Persons.length+1}}">{{position.InstructionNumber}}{{position.InstructionNumber=='' ? '' : <?="' ".Module::t('ML','from')." '"?>+position.InstructionDate}}
                            <?php if(ACLRole::accessControllerAction('create','employeeorder')):?>
                            <?= AnguHtml::spanButton("glyphicon glyphicon-pencil", null,
                                ['title' => Module::t('ML','Staff Instruction'),
                                        'ng-hide' => "position.Modified > 0",
                                        'ng-click' => "updatePosition(position,1)",
                                        'class' => 'pointer'], 'create', 'employeeorder') ?> 
                            <?= AnguHtml::spanButton("glyphicon glyphicon-trash", null,
                                ['title' => Module::t('ML','Delete'),
                                        'ng-hide' => "position.Modified > 0 || position.InstructionNumber==''",
                                        'ng-click' => "deleteInstruction(position)",
                                        'class' => 'pointer'], 'create', 'employeeorder') ?>
                            <?php endif;?>
                        </td>
                        <td ng-class="{{classForTrTd}}" rowspan="{{position.Persons.length+1}}">
                            <div ng-show="position.InProcess == 0">
                                <?php if(ACLRole::accessControllerAction('update')):?>
                                <?= AnguHtml::spanButton("glyphicon glyphicon-pencil", null,
                                    [
                                        'title' => Module::t('ML','Update'),
                                        'ng-hide' => "position.StatusDel == 1",
                                        'ng-click' => "updatePosition(position,0)",
                                        'class' => 'pointer'
                                    ],'create') ?> 
                                <?php endif;?>
                                <?php if(ACLRole::accessControllerAction('view','staffgeneralorder')):?>
                                <?= AnguHtml::spanButton("glyphicon glyphicon-file", "/orders/staffgeneralorder/view?id={{position.GeneralOrderID}}",
                                    [
                                        'title' => Module::t('ML','Order'),
                                        'target' => "_blank",
                                        'ng-show' => "position.Modified > 0",  
                                        'class' => 'pointer'                                 
                                    ],'view', 'staffgeneralorder') ?>
                                <?php endif;?>
                                <?php if(ACLRole::accessControllerAction('create')):?>
                                <?= AnguHtml::spanButton("glyphicon glyphicon-trash", null,
                                    [
                                        'title' => Module::t('ML','Delete'),                                    
                                        'ng-show' => "position.Persons[0].PersonID === undefined",
                                        'ng-click' => "deletePosition(position.ID)",
                                        'class' => 'pointer'
                                    ],'create') ?>
                                <?php endif;?>
                                <?php if(ACLRole::accessControllerAction('create','employeeorder')):?>
                                <?= AnguHtml::spanButton("glyphicon glyphicon-plus", "/ok/employeeorder/create?staffPositionParentID={{position.ParentID}}",
                                    [
                                        'title' => Module::t('ML','Employ'), 
                                        'target' => "_blank", 
                                        'ng-show' => "position.Value - (position.HoldWOAbsent > 0 ? position.HoldWOAbsent : 0) > 0 && position.Modified == 0"
                                    ],'create', 'employeeorder') ?>  
                                <?php endif;?>
                            </div>
                        </td>                                 
                    <tr ng-repeat="person in position.Persons">
                        <td ng-if="!$first" ng-class="{fontsel : person.EmployeeRaw == 1, 
                                fontrawadd : person.EmpInProcess > 0 && person.EmpExit==0, 
                                fontdel: person.EmpExit > 0,
                                fontabsent: person.EmpInProcess == 0 && person.EmployeeStateID == 100391,
                                fontparttime: person.EmpInProcess == 0 && person.EmployeeStateID == 100392}">{{person.Person}} {{person.PersonValue}} {{person.BeforeVacationValue > 0 ? '('+person.BeforeVacationValue+')' : ''}}
                            <div ng-show="ProtocolMode"><p><i>{{protocolInfo(person.EmployeeParentID)}}</i></p></div>
                            <?php if(ACLRole::accessControllerAction('create','employeeorder')):?>
                            <?= AnguHtml::spanButton("glyphicon glyphicon-file", "/ok/employeeorder/create?employeeParentID={{person.EmployeeParentID}}",
                                ['title' => Module::t('ML', 'Create {modelClass}', [
				'modelClass' => Module::t('ML', 'Employee Order')]),
                                'target' => "_blank",
                                'ng-show' => "person.EmployeeRaw == 0 && person.EmpInProcess == 0"], 'create', 'employeeorder') ?> 
                            <?php endif;?>
                            <?php if(ACLRole::accessControllerAction('index','employee')):?>
                            <?= AnguHtml::spanButton("glyphicon glyphicon-eye-open", "/ok/employee/index?parentid={{person.EmployeeParentID}}",
                                ['title' => Module::t('ML', 'View'), 
                                'target' => "_blank", 
                               // 'ng-show' => "person.EmployeeRaw == 1 || person.EmpInProcess > 0"
                                    ], 'index', 'employee') ?> 
                            <?php endif;?>
                            <?php if(ACLRole::accessControllerAction('create','employeeorder')):?>
                            <?= AnguHtml::spanButton("glyphicon glyphicon-list", null,
                                ['title' => Module::t('ML', 'Protocol'),
                                 'ng-show' => "person.EmployeeRaw == 0 && person.EmpInProcess == 0",
                                 'ng-click' => "showProtocol(person,position)",
                                 'class' => 'pointer',
                                    ], 'index', 'employee') ?> 
                            <?php endif;?>
                        </td>
                    </tr>
                    <tr ng-if="position.Selected"> 
                        <td ng-if="position.InProcess > 0 || position.ActualParentID > 0"/>
                        <td ng-if="!position.InstructionMode" colspan="10">
                            <?= $this->render('_form',['baseModelsNamespace' => $baseModelsNamespace]);?>
                        </td>
                        <td ng-if="position.InstructionMode" colspan="10">
                            <?= $this->render('_formInstruction',['baseModelsNamespace' => $baseModelsNamespace]);?>
                        </td>
                    </tr>
                    <tr ng-repeat-end/>
                    <tr class="selnode">
                        <td />
                        <td colspan="4"><?= Module::t('ML', 'Total') ?></td>
                        <td>{{(resultArray | sum : 'Value').toFixed(2)}}</td>
                        <td>{{hwoa = (resultArray | sum : 'HoldWOAbsent').toFixed(2)}}{{((hwa = (resultArray | sum : 'HoldWithAbsent').toFixed(2)) == hwoa) ? '' : ' ('+hwa+')'}}</td>                    
                        <td>{{free = ((resultArray | sum : 'Value') - (resultArray | sum : 'HoldWOAbsent')).toFixed(2)}}{{((freewa = ((resultArray | sum : 'Value') - (resultArray | sum : 'HoldWithAbsent')).toFixed(2)) == free) ? '' : ' ('+freewa+')'}}</td>
                        <td colspan="3"/>                   
                    </tr>
                </tbody>
            </table>
        </div>
        
    </div>	
</div>
