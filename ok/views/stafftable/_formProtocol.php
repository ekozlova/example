<?php
use common\components\AnguHtml;
use common\modules\roles\models\ACLRole;
use common\modules\ok\Module;
use common\modules\ok\models\Employee;
use yii\helpers\ArrayHelper;

$employee = new Employee;
?>
<h4 style="font-weight:bold;">{{position.selectedPerson.Person}} {{position.selectedPerson.PersonValue}}ст.</h4>
<h5>{{position.Post}}, {{position.Rate}}</h5>
       <form name="formProtocol">     
       <div class="row"  style="padding:20px;">
           <?= AnguHtml::textInput($employee, 'ProtocolNumber', ['ng-model' => 'position.selectedPerson.protocol.ProtocolNumber']) ?>       
        
           <?= AnguHtml::dateInput($employee, 'ProtocolDate', ['ng-model' => 'position.selectedPerson.protocol.ProtocolDate']) ?>      
        
           <?= AnguHtml::dropDownList($employee, 'DecisionTypeID', ['ng' => ['model' => 'position.selectedPerson.protocol.DecisionTypeID', 'repeat' => 'decisionTypes']]) ?> 
       </div>
          
       <div style="clear:left; float:right">
           
           <?= AnguHtml::aButton(Module::t('ML', isset($isNewRecord) && $isNewRecord ? 'Create' : 'Update'),
                ['btnType' => 'primary',                    
                 'ng-click' => "updateProtocol(position)",                
                 'ng-disabled' => "formProtocol.\$invalid",
                 ]) ?>
           <?= AnguHtml::aButton(Module::t('ML', 'Hide'),
                ['btnType' => 'default',                    
                 'ng-click' => "cancelProtocol(position)",
                 ]) ?>
       </div>      
       </form>    
