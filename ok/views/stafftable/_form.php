<?php
use common\components\AnguHtml;
use common\modules\roles\models\ACLRole;
use common\modules\ok\Module;

if (!isset($itemName)) {
    $itemName = 'position';
    $prefix = '-{{position.ID}}';    
} else $prefix = ''; 
$prefixName = str_replace('-', '_', $prefix);
?>
       <form name="form<?= $prefixName ?>">     
       <div class="col-xs-6">
           <?= AnguHtml::autoCompleteField($baseModelsNamespace.'Job', 'PostID', [
                   'id' => "position{$prefix}-posts",                                        
                   'initial-value' => "{{{$itemName}.Post}}",
                   'disable-input'=>"'{$itemName}'!='newposition'",                         
                   //'field-required' => true,
                          // 'field-required-class' => 'number',
           ], $this) ?>           
           <?= AnguHtml::dropDownList($baseModelsNamespace.'Job', 'RateID', [
               'ng' => [
                   'model' => "{$itemName}.changed.Job.RateID",
                   'repeat' => "(rates | filter : {PostID : {$itemName}.changed.Job.PostID} : true )",
                   'key' => 'ParentID'],
                'required' => true,
                'ng-disabled' => "'{$itemName}'!='newposition'",           
               ])?> 
           <?= AnguHtml::dropDownList($baseModelsNamespace.'StaffPosition', 'StaffCategoryID', [
               'ng' => [
                   'model' => "{$itemName}.changed.StaffPosition.StaffCategoryID",
                   'repeat' => 'categories',],
                'required' => true,
                'ng-disabled' => "'{$itemName}'!='newposition'",
               ])?>            
       </div>
       <div class="col-xs-6"> 
           <?= AnguHtml::dropDownList($baseModelsNamespace.'StaffPosition', 'FinanceTypeID', [
               'ng' => [
                   'model' => "{$itemName}.changed.StaffPosition.FinanceTypeID",
                   'repeat' => 'financeTypes'],
                'required' => true, 
                'ng-disabled' => "'{$itemName}'!='newposition'",           
               ])?> 
           <div class="row">
           <div class="col-xs-6">
           <?= AnguHtml::textInput($baseModelsNamespace.'StaffPosition', 'Value', ['ng-model' => "{$itemName}.changed.StaffPosition.Value", 
                   'name' => 'position'.$prefixName.'_value', 
                   'required' => true,
                   'disabled' => !ACLRole::checkAccess('staff_table') && !ACLRole::checkAccess('admin'),
                  // 'div' => ['ng-class' => '{"has-error": form'.$prefixName.'.position'.$prefixName.'_value.$invalid}']
                  ])?>
           </div>
           <div class="col-xs-6">
           <?=isset($isNewRecord) && $isNewRecord ? AnguHtml::dateInput($baseModelsNamespace.'StaffPosition', 'StartDate', ['ng-model' => "{$itemName}.changed.StaffPosition.StartDate"]): ''?>
           </div>
           </div>           
           </div>
       <fieldset class="lite col-xs-12">   
           <legend style="font-size:18px; background-image: none; font-weight: normal; margin: 0; width: auto"><?= Module::t('ML', 'Increases')?></legend> 
           <?= AnguHtml::aButton('{{'.$itemName.'.addincrease ? "' . Module::t('ML', 'Hide') . '" : "' . Module::t('ML', 'Add') . '"}}',
                   ['ng-click' => "addIncrease({$itemName})",'btnType' => 'default', 'ng-show' => $itemName.'.changed.Increases.length == 0', 
                           'style' => 'float: right'])?>       
       <fieldset class="lite" ng-repeat="increase in <?= $itemName ?>.changed.Increases">                 
       <div class="col-xs-6">
           <?= AnguHtml::dropDownList($baseModelsNamespace.'StaffPositionIncrease', 'IncreaseTypeID', [
               'ng' => [
                   'model' => "increase.IncreaseTypeID",
                   'repeat' => 'increaseTypes',], 'disabled' => !ACLRole::checkAccess('staff_table'),
              ])?>
       </div>
       <div class="col-xs-6">    
       <div class="col-xs-8">
           <?= AnguHtml::textInput($baseModelsNamespace.'StaffPositionIncrease', 'Sum', 
                ['ng-model' => "increase.Sum",
                 'div' => ['ng-show' => "showFixed(increase.IncreaseTypeID)"], 'disabled' => !ACLRole::checkAccess('staff_table'),])?>
           <?= AnguHtml::textInput($baseModelsNamespace.'StaffPositionIncrease', 'BaseRateMultiplier', 
                ['ng-model' => "increase.BaseRateMultiplier",
                 'div' => ['ng-hide' => "showFixed(increase.IncreaseTypeID)"], 'disabled' => !ACLRole::checkAccess('staff_table'),])?>
       </div>           
       <div class="col-xs-4" style="padding-top: 25px">
           <?= AnguHtml::aButton('{{'.$itemName.'.addincrease ? "' . Module::t('ML', 'Hide') . '" : "' . Module::t('ML', 'Del') . '"}}',
                   ['ng-click' => "delIncrease({$itemName},\$index)",'btnType' => 'default'])?>
           <?= AnguHtml::aButton('{{'.$itemName.'.addincrease ? "' . Module::t('ML', 'Hide') . '" : "' . Module::t('ML', 'Add') . '"}}',
                   ['ng-click' => "addIncrease({$itemName})",'btnType' => 'default', 'ng-show' => '$last'])?>           
       </div>
       </div>    
       </fieldset>   
       </fieldset>     
       <div style="clear:left; float:right">
           <?= !isset($isNewRecord) || !$isNewRecord ? AnguHtml::aButton(Module::t('ML',  'Change {model} only', ['model' => Module::t('ML','Finance Type')]),
                ['btnType' => 'default',                    
                 'ng-click' => "updateFinanceType({$itemName},\$event)",                
                 'ng-disabled' => "form".$prefixName.".\$invalid",
                 'ng-hide' => "{$itemName}.Modified == 1",        
                 ]) : '' ?>
           <?= AnguHtml::aButton(Module::t('ML', isset($isNewRecord) && $isNewRecord ? 'Create' : 'Update'),
                ['btnType' => 'primary',                    
                 'ng-click' => "updatePositionItems({$itemName},\$event)",                
                 'ng-disabled' => "form".$prefixName.".\$invalid",
                 ]) ?>
       </div>      
       </form>    
