<?php

namespace common\modules\ok;

use Yii;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'common\modules\ok\controllers';
    public $defaultRoute = 'stafftable';
    
    public $keyPrefix = '__ok__';
    public $cacheTime = 31536000;

    public function init()
    {
        parent::init();
        $this->registerTranslations();
        // custom initialization code goes here
    }
    public function registerTranslations(){
        Yii::$app->i18n->translations['ML*'] = [
            'class' => 'yii\i18n\PhpMessageSource',
            'basePath' => '@common/modules/ok/messages',
            'fileMap' => [
                'ML' => 'ML.php'
            ],
        ];
    }
    
    public static function t($category, $message, $params = [], $language = null){
        return Yii::t($category, $message, $params, $language);
    }
}
