var stafftable = angular.module('stafftable',['ngResource', 'ya.treeview.object', 'angucomplete-alt']);

stafftable.factory('suggest', ['$resource', 
  function($resource) {
    return $resource('/ok/stafftable/:action',{},{
        get: {params : {action : 'suggest'}},// получение json для дерева
        query: {params : {action : 'getpositions'}},
        save: {params : {action: 'saveposition'}, method : 'POST'},
        delete: {params: {action: 'deleteposition'}, method : 'POST'},
        getProtocol: {params : {action : 'getprotocol'}},
        saveProtocol: {params : {action : 'saveprotocol'}, method : 'POST'},
    });
}]);

stafftable.filter('sum', function(){
  return function(array, key) {
   if (array == undefined || array.length == 0) return 0;
    var sum = 0;
    array.forEach(function(item){if (item[key]!==undefined && item.Modified==0) sum += parseFloat(item[key]);});
    return sum;
  };
  
 });

stafftable.controller('stafftableController', ['$scope', 'suggest', '$filter', function($scope, suggest, $filter){
   
  $scope.model = []; 
  $scope.reportParams = '';
  
  $scope.context =  {selectedNodes: []}; 
  
  $scope.options = {
    
      onSelect: function($event, node, context) {
          if ($scope.departmentParentID!=node.$model.ParentID)
                      positionsSetting(node.$model.ParentID);
                  
              if (context==null) $scope.context.selectedNodes = [node];
              else {
                  context.selectedNodes = [node];
              var pth = node.$model.ParentID;
              var p = node.$parent; 
              while (p!==undefined && p!==null && p.$parent!==null){                  
                  pth += '-'+p.$model.ParentID;
                  p = p.$parent;
              }            
              location.hash = pth;}
          $scope.departmentParentID = node.$model.ParentID;     
          $scope.reportParams = '&DepartmentParentID='+node.$model.ParentID;
      },     
  };
         
  var token = GLOBAL.WaitAnimation.show();
  suggest.get({},function(msg){$scope.error = '';
      $scope.model = msg.result;
      GLOBAL.WaitAnimation.hide(token);
  },function(msg){
  $scope.error = msg.data.message; 
  GLOBAL.WaitAnimation.hide(token);});
  
  $scope.departmentParentID = $scope.context.selectedNodes[0]!==undefined ? $scope.context.selectedNodes[0].$model.ParentID : undefined;  

  $scope.newposition = {Selected : false, changed : {
        StaffPosition : {
                  StaffCategoryID : undefined, 
                  FinanceTypeID : undefined, 
                  Value : 0,
                  StartDate : ''}, 
        Job : {
            PostID : 0, 
            RateID : 0
        }, 
        Increases : [{IncreaseTypeID : undefined}],
        Instruction : {Number : '', Date : '', Text : ''}, 
        InstructionID : 0}};  
  
  $scope.globalMode = false;
  $scope.classForTrTd = '{typerawadd: position.Modified > 0 && position.ActualParentID > 0 && position.ParentValue == position.Value && position.StatusDel == 0, typelike: position.Modified > 0 && position.ActualParentID == 0 || position.Modified > 0 && position.ActualParentID > 0 && position.ParentValue < position.Value, typedel: position.StatusDel > 0|| position.Modified > 0 && position.ActualParentID > 0 && position.ParentValue > position.Value}';
  $scope.showschema = false;
  $scope.flt = {StaffCategoryID : 0, PostID : 0, FinanceTypeID : 0};
  $scope.AllProtocols = [];
  $scope.ProtocolsCopy = [];
  $scope.ProtocolMode = false;
  
  $scope.updatePosition = function(position, instructionMode){
      position.Selected = !position.Selected;
      position.InstructionMode = instructionMode > 0;
      if (position.changed === undefined) {
          initPositionChanged(position);
      }     
   };
   
  $scope.deletePosition = function(positionid){
      if (confirm('Вы уверены, что хотите удалить данный элемент?')){
          suggest.delete({id : positionid},function(msg){$scope.error = '';              
              positionsSetting($scope.departmentParentID);
          },function(msg){$scope.error = msg.data.message;});
      }
  };
  
  $scope.addPosition = function(position){
      position.Selected = !position.Selected;      
  };
    
  $scope.updatePositionItems = function(position,event,instr){
  
      var str = (angular.element(event.target))[0].outerHTML;
      if (str.indexOf('disabled="disabled"')>-1) {return;}
      
      var original = clonePosition(position.changed);
      
      if (position.changed.InstructionID == 0) delete(position.changed['InstructionID']);
      if (instr == undefined && position.changed.Instruction != undefined) delete(position.changed['Instruction']);
      
      
      for (var key in position.changed){
        if (!angular.isObject(position.changed[key])){
             if (position.changed[key] === position[key]) delete(position.changed[key]);
        }  
        else {
            for (var key2 in position.changed[key]){
                 if (position.changed[key][key2] === '' || position.changed[key][key2] === position[key2]) delete(position.changed[key][key2]);
                      }
             if (Object.keys(position.changed[key]).length == 0) delete(position.changed[key]);
        }
       }   
       
        //console.log(position.changed);
        position.changed.spID = position.ID;
        if (position.ID === undefined && position.changed.StaffPosition!== undefined)
            position.changed.StaffPosition.DepartmentParentID = $scope.departmentParentID;
        var token2 = GLOBAL.WaitAnimation.show();
        suggest.save(position.changed,function(msg){$scope.error = '';
                    //position.changed = {};
                    position.changed = {
                            StaffPosition : {
                                      StaffCategoryID : undefined, 
                                      FinanceTypeID : undefined, 
                                      Value : 0,
                                      StartDate : ''}, 
                            Job : {
                                PostID : 0, 
                                RateID : 0
                            }, 
                            Increases : [{IncreaseTypeID : undefined}],
                            Instruction : {Number : '', Date : '', Text : ''}, 
                            InstructionID : 0};
                    //$scope.model = msg.result;
                    position.Modified = 1;
                    positionsSetting($scope.departmentParentID);
                    GLOBAL.WaitAnimation.hide(token2);                    
                },function(msg){
                $scope.error = msg.data.message; 
                GLOBAL.WaitAnimation.hide(token2);
                position.changed = original;
                position.Selected = true;
            });
        position.Selected = false;
  }  
  
  $scope.selectItem = function(item, name, prefix, obj){   
      var itemName = (obj.$parent.$parent.position === undefined) ? 'newposition' : 'position';      
      
      if (item !== undefined && item.originalObject.id !== obj.$parent.$parent[itemName][name]){
        if (prefix !== undefined && prefix.length > 0)  
             obj.$parent.$parent[itemName].changed[prefix][name] = item.originalObject.id;
        else obj.$parent.$parent[itemName].changed[name] = item.originalObject.id;
    
       var newName = name.replace(/ID/g,"");
          if (obj.$parent.$parent[itemName][newName] === undefined) obj.$parent.$parent[itemName][newName] = item.originalObject.label;
      }
      
  };
  
  $scope.selectPost = function(item){
      return $scope.selectItem(item,'PostID','Job',this);      
  };
   
  $scope.selectInstruction = function(item){
      return $scope.selectItem(item,'InstructionID','',this);      
  };
  
  $scope.selectDepartmentParent = function(item){
      searchTreeDepartment(item);
  };
  
  $scope.selectPerson = function(item){
      searchTreeDepartment(item);
  };
  
  $scope.addInstruction = function(position){
      position.addinstruction = !position.addinstruction;
        
  };
  
  $scope.showFixed = function(id){
      var elem = findByID($scope.increaseTypes,id);     
      return (elem.Fixed == 1);
  };
    
  $scope.addIncrease = function(position){
     position.changed.Increases.push({});
  };
  
  $scope.delIncrease = function(position,index){
     position.changed.Increases.splice(index,1);
  };
  
  $scope.updateFinanceType = function(position,event){
      var str = (angular.element(event.target))[0].outerHTML;
      if (str.indexOf('disabled="disabled"')>-1) {return;}
      initPositionChanged(position);
      var newPosition = clonePosition(position.changed);
      newPosition.StaffPosition.FinanceTypeID = 3 - position.changed.StaffPosition.FinanceTypeID;
      newPosition.StaffPosition.DepartmentParentID = $scope.departmentParentID;
      newPosition.spID = null;
      
      var token3 = GLOBAL.WaitAnimation.show();
      suggest.delete({id : position.ID},function(msg){$scope.error = '';   
      suggest.save(newPosition,function(msg){$scope.error = '';
                    position.changed = {};
                    //$scope.model = msg.result;
                    position.Modified = 1;
                    positionsSetting($scope.departmentParentID);
                    GLOBAL.WaitAnimation.hide(token3);                    
                },function(msg){
                $scope.error = msg.data.message; 
                GLOBAL.WaitAnimation.hide(token3);
                position.Selected = true;
            });    
                
              positionsSetting($scope.departmentParentID);
          },function(msg){$scope.error = msg.data.message;
                          GLOBAL.WaitAnimation.hide(token3);});  
      
  };
  
  $scope.setInfo = function(position){
      position.Text = 'Дата введения ставки: '+position.StartDate;
      position.Info = !position.Info;     
  };
  
/*  $scope.fltr = function(position){
      var fl = true;
      for (var key in $scope.flt){
          fl = fl && ($scope.flt[key] == 0|| $scope.flt[key] == position[key]);
      }
      
      return fl;
  };*/
  
  $scope.deleteInstruction = function(position){
        var token2 = GLOBAL.WaitAnimation.show();
        suggest.save({spID : position.ID, Instruction : {delete : 1}},function(msg){$scope.error = '';                    
                    positionsSetting($scope.departmentParentID);
                    GLOBAL.WaitAnimation.hide(token2);                    
                },function(msg){
                $scope.error = msg.data.message; 
                GLOBAL.WaitAnimation.hide(token2);
            });
  };
  
  $scope.showProtocol = function(person,position){  
      if ($scope.AllProtocols.length == 0){
        suggest.getProtocol({department : $scope.departmentParentID}, function(msg){
            $scope.AllProtocols = msg.result;            
            person.protocol = $scope.AllProtocols[person.EmployeeParentID];      
            var oldprotocol = {
          ProtocolNumber : person.protocol.ProtocolNumber, 
          ProtocolDate : person.protocol.ProtocolDate, 
          DecisionTypeID : person.protocol.DecisionTypeID,
      };
      person.protocol.oldprotocol = oldprotocol;
        });}
      else{
        person.protocol = $scope.AllProtocols[person.EmployeeParentID];
        var oldprotocol = {
          ProtocolNumber : person.protocol.ProtocolNumber, 
          ProtocolDate : person.protocol.ProtocolDate, 
          DecisionTypeID : person.protocol.DecisionTypeID,
      };
      person.protocol.oldprotocol = oldprotocol;
      } 
      position.selectedPerson = person;
      position.ProtocolMode = true;
      
  };
  
  $scope.updateProtocol = function(position){
      if (position.selectedPerson == undefined) return;
      
      position.selectedPerson.protocol.ParentID = position.selectedPerson.EmployeeParentID;
      suggest.saveProtocol({Employee : position.selectedPerson.protocol}, 
            function(msg){
                position.ProtocolMode = false;
                $scope.AllProtocols[position.selectedPerson.EmployeeParentID] = position.selectedPerson.protocol;
                }, 
            function(msg){});
  };
  
  $scope.cancelProtocol = function(position){
      position.ProtocolMode = false;
      $scope.AllProtocols[position.selectedPerson.EmployeeParentID] = position.selectedPerson.protocol.oldprotocol;
  };
  
  $scope.protocolInfo = function(employeeParentID){
        var pr = $scope.AllProtocols[employeeParentID];
        return pr!= undefined && pr.ProtocolNumber!='' ? 'Пр. '+ pr.ProtocolNumber+' от '+ pr.ProtocolDate + ' '+(pr.DecisionTypeID == 1 ? ' реш.Уч.С.' : (pr.DecisionTypeID == 2 ? ' реш.Уч.С.Ф.' : '')) : '';   
  };
  
  $scope.showAllProtocols = function(){
    if ($scope.AllProtocols.length == 0){
        suggest.getProtocol({department : $scope.departmentParentID}, function(msg){
            $scope.AllProtocols = msg.result;      
                   });}
    $scope.ProtocolMode = !$scope.ProtocolMode;         
  };
          
  function positionsSetting(parentId){
      suggest.query({parentId : parentId}).$promise.then(function(msg){
          $scope.AllProtocols = [];
          $scope.ProtocolMode = false;
          $scope.positions = [];
          $scope.posts = [];
          var postids = [];
          for (var key in msg){
              if (isObject(msg[key])){
                    $scope.positions.push(msg[key]);
                    if (msg[key].PostID!==undefined && postids[msg[key].PostID] === undefined)
                    $scope.posts.push({ID : msg[key].PostID, Name : msg[key].Post});
                    postids[msg[key].PostID] = 1;
              }
          }          
          
});
  };
  
  function treeSetting(){
  var token = GLOBAL.WaitAnimation.show();
  suggest.get({},function(msg){$scope.error = '';
      $scope.model = msg.result;
      GLOBAL.WaitAnimation.hide(token);
  },function(msg){
  $scope.error = msg.data.message; 
  GLOBAL.WaitAnimation.hide(token);});
 }
  
  function clonePosition(item){
      var increases = [];
      if (item.Increases !== undefined){
          increases = item.Increases.slice();
      }
      
      return {
              StaffPosition : {
                  StaffCategoryID : item.StaffPosition.StaffCategoryID, 
                  FinanceTypeID : item.StaffPosition.FinanceTypeID, 
                  Value : item.StaffPosition.Value},
              Job : { 
                  PostID : item.Job.PostID, 
                  RateID : item.Job.RateID,
                  Post : item.Post, 
                  Rate : item.Rate},
              Increases : increases,
              InstructionID : item.InstructionID,
              Instruction : {Number : item.Instruction.Number, Date : item.Instruction.Date, Text : item.Instruction.Text},
          };
  };
  
  function findByID(array,value){
      var elem = $filter('filter')(array,{ID : value}, true);
      return elem.length > 0 ? elem[0] : {};
  };
  
  function initPositionChanged(position){
              position.changed = {
              StaffPosition : {
                  StaffCategoryID : position.StaffCategoryID, 
                  FinanceTypeID : position.FinanceTypeID, 
                  Value : position.Value},
              Job : { PostID : position.PostID, RateID : position.RateID},             
              InstructionID : position.InstructionID,
              Instruction : {Number : position.InstructionNumber, Date : position.InstructionDate, Text : ''},
          };       
          position.changed.Increases = position.Increases.slice();
          
          };     
   
   function searchTreeDepartment(item){
      if (item !== undefined){
            var td = item.originalObject.TreeDepartment;
            location.hash = td.split('::').reverse().join('-');
            treeSetting();
    }    
  };
}]);