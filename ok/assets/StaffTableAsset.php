<?php

namespace common\modules\ok\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class StaffTableAsset extends AssetBundle
{
    public $sourcePath = __DIR__;
    
    public $js = [
        'js/stafftable.js',
    ];
    
    public $jsOptions = ['position' => \yii\web\View::POS_END];
    
    public $publishOptions = [
        'forceCopy'=>true,
    ];    
}
