<?php

namespace common\modules\ok\controllers;

use Yii;
use common\components\AisUniversityController as Controller;
use yii\web\NotFoundHttpException;
use common\modules\roles\models\ACLRole;
use common\modules\ok\models\StaffTable;
use common\modules\ok\models\StaffCategory;
use common\modules\ok\models\FinanceType;
use common\modules\ok\models\IncreaseType;
use common\modules\ok\models\Rate;
use common\modules\ok\models\RateLabel;
use common\modules\ok\models\Job;
use common\modules\department\models\Department;
use common\modules\ok\models\Employee;
use common\modules\ok\models\StaffPosition;
use common\modules\ok\models\DecisionType;

/**
 * EmployeeController implements the CRUD actions for Employee model.
 */
class StafftableController extends Controller
{
    public $enableCsrfValidation = false;
    
    public function behaviors()
    {
        return ACLRole::defaultBehaviors();
    }

    /**
     * Lists all.
     * @return mixed
     */
    public function actionIndex()
    {
        $categories = StaffCategory::find()->asArray()->all();
        $financeTypes = FinanceType::find()->asArray()->all();
        $increaseTypes = IncreaseType::find()->asArray()->all();
        $rates = Job::find()->select(['PostID',Rate::tableName().'.ParentID',RateLabel::tableName().'.Label as Name'])->joinWith(['rate.rateLabel'],false)->asArray()->all();
        $decisionTypes = DecisionType::find()->asArray()->all();
        
        return $this->render('index' ,['categories' => $categories, 
            'financeTypes' => $financeTypes, 
            'increaseTypes' => $increaseTypes, 
            'rates' => $rates,
            'decisionTypes' => $decisionTypes,
        ]);
    }
    
    public function actionSuggest()
    {
        Yii::$app->response->format = 'json';
        return StaffTable::getDepartmentTree();
    }
    
    public function actionGetpositions($parentId = 0)
    {
        Yii::$app->response->format = 'json';
        return StaffTable::getPositions($parentId);
    }
    
    public function actionSaveposition()
    {
        Yii::$app->response->format = 'json';
        $attributes = json_decode(Yii::$app->request->getRawBody(),true);
        $result = StaffTable::savePosition($attributes);
        
        return $result;
    }
    
    public function actionDeleteposition()
    {
        Yii::$app->response->format = 'json';
        $attributes = json_decode(Yii::$app->request->getRawBody(),true);
        $result = StaffTable::deletePosition($attributes);
      
        return $result;
    }
       
    public function actionSuggesttree($term){
        if(strlen($term) > 1) {
            $query = Department::find()->andWhere(['like',"name",trim($term)]);   
            $query->limit(Yii::$app->params['maxSuggest']);
            $models = $query->all();
            $output = [];
            
            foreach ($models as $model) {
                $elem['label'] = $model->Name;
                $elem['value'] = $model->Name;
                $elem['id'] = $model->ParentID;
                $elem['TreeDepartment'] = $model->TreeDepartment;
                //$elem['Parent'] = 
                $output[] = $elem;
            }
            echo json_encode(['results'=>$output]);
		} else {
			throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    public function actionSuggestperson($term, $short = false){
        if(strlen($term) > 3) {
            $e = Employee::tableName();        
            $sp = StaffPosition::tableName();
            $query = Employee::find()->joinWith(['passport','staffPosition.department','staffPosition.job.post']);
           
            $query->andWhere(["like", "LastName + ' ' + FirstName + ' ' + MiddleName + ' ' + CAST (BirthDate as varchar)", trim($term)]);
            $query->andWhere('EmployeeStateID <> 100390');
            $query->andWhere(["$e.IsRaw" => 0]);
            $query->andWhere(["$sp.IsRaw" => 0]);
            $query->limit(Yii::$app->params['maxSuggest']);
            $models = $query->all();
            $output = [];
            
            foreach ($models as $model) {
                if ($short) {
                    $elem['Department'] = $model->staffPosition->department->Name;
                    $elem['Post'] = $model->staffPosition->job->post->Name;                    
                    $elem['label'] = $model->PersonHRwoBD.' ('.$model->staffPosition->shortName.')';
                    $elem['value'] = $model->PersonHRwoBD;
                    $elem['id'] = $model->ParentID;
                }
                else {
                $elem['label'] = $model->Name;
                $elem['value'] = $model->PersonHRwoBD;
                $elem['id'] = $model->PersonID;
                $elem['TreeDepartment'] = $model->staffPosition->department->TreeDepartment;
                //$elem['Parent'] = 
                }
                $output[] = $elem;
            }
            echo json_encode(['results'=>$output]);
		} else {
			throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    public function actionSuggestemployee($term){
        return $this->actionSuggestperson($term, true);
    }
    
    public function actionGetprotocol($department){
        Yii::$app->response->format = 'json';
        return Employee::getProtocol($department);
    }
    
    public function actionSaveprotocol(){
        Yii::$app->response->format = 'json';
        $attributes = json_decode(Yii::$app->request->getRawBody(),true);
        $result = Employee::saveProtocol($attributes);
        
        return $result;
    }
}
