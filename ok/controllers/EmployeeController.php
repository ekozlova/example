<?php

namespace common\modules\ok\controllers;

use Yii;
use common\modules\ok\models\Employee;
use common\modules\ok\models\searches\EmployeeSearch;
use common\modules\department\models\Department;
use common\components\AisUniversityController as Controller;
use yii\web\NotFoundHttpException;
use common\modules\roles\models\ACLRole;
use common\modules\ok\Module;

/**
 * EmployeeController implements the CRUD actions for Employee model.
 */
class EmployeeController extends Controller
{
    public function behaviors()
    {
        return ACLRole::defaultBehaviors();
    }

    /**
     * Lists all Employee models.
     * @return mixed
     */
    public function actionIndex($parentid = 0, $personID = 0)
    {
        $searchModel = new EmployeeSearch();      
        if($parentid != 0){
            $dataProvider[] = [ 'dp' => $searchModel->search(['EmployeeSearch'=>['ParentID' => $parentid]])];
            $fullHistory = false;
        } else 
        if($personID != 0){
            $dataProvider = [];
            $models = Employee::find()->andWhere(['PersonID'=>$personID, 'IsRaw'=>1])->andWhere(['is','DeletedDate',null])->andWhere(['not in','ParentID',Employee::find()->select('ParentID')->distinct()->andWhere(['PersonID'=>$personID, 'IsRaw'=>0])->andWhere(['is','DeletedDate',null]) ])->orderBy('VersionDate desc, ID desc')->all();
            foreach ($models as $model){
                $dataProvider[] = [ 'ttl'=>Module::t('ML', 'In Process'),'class'=>'three', 'dp' => $searchModel->search(['EmployeeSearch'=>['ParentID' => $model->ParentID]])];
            }
            $models = Employee::find()->andWhere(['PersonID'=>$personID, 'IsRaw'=>0])->andWhere(['is','DeletedDate',null])->orderBy('VersionDate desc, ID desc')->all();
            foreach ($models as $model){
                $dataProvider[] = [ 'ttl'=>Module::t('ML', 'Employee').' '.Yii::t('ML', 'from').' '.date('d.m.Y',strtotime($model->StartDate)).( $model->employeeState->Label == 'valid' ? '' : ' '.Yii::t('ML', 'to').' '.date('d.m.Y',strtotime($model->StartDate)) ), 'class'=>$model->employeeState->Label == 'valid' ? 'one' : 'two' , 'dp' => $searchModel->search(['EmployeeSearch'=>['ParentID' => $model->ParentID]])];
            }
            $fullHistory = true;
        }
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'fullHistory' => $fullHistory,
        ]);
    }

    /**
     * Displays a single Employee model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id), 
        ]);
    }

    /**
     * Creates a new Employee model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Employee();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->ID]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Employee model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->ID]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Employee model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Employee model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Employee the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Employee::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
	
	/**
	 * @param type $term
	 * @return mixed
	 */
	public function actionSuggest($term) {
		if(strlen($term) > 1) {
			\Yii::$app->response->format = 'json';
			
			$pAlias = \common\modules\persons\models\Passport::tableName();
			$query = Employee::find()
					->joinWith(['person.passport'], false)
					->limit(Yii::$app->params['maxSuggest']);
			
			foreach(explode(' ', $term) as $value) {
				$query->andWhere(["LIKE", "{$pAlias}.LastName + ' ' + {$pAlias}.FirstName + ' ' + {$pAlias}.MiddleName + ' ' + CAST({$pAlias}.BirthDate as varchar)", trim($value)]);
			}
			
			return \common\components\Helper::objectToArray($query->all(), ['ID' => 'id', 'Name' => 'label', 'PersonHRwoBD' => 'value']);
		} else {
			throw new NotFoundHttpException('The requested page does not exist.');
		}
	}
        
    public function actionSuggestnew($DepartmentParentID) {
        \Yii::$app->response->format = 'json';
        $e = Employee::tableName();
        $es = \common\modules\ok\models\EmployeeState::tableName();
        $p = \common\modules\persons\models\Person::tableName();
        $c = \common\modules\persons\models\Contact::tableName();
        $pas = \common\modules\persons\models\Passport::tableName();
        $pt = \common\modules\ok\models\Post::tableName();
        $d = Department::tableName();
        $sp = \common\modules\ok\models\StaffPosition::tableName();
        $TreeDepartment = Department::find()->andWhere(['ParentID'=>$DepartmentParentID])->one()->TreeDepartment;
        return Employee::find()
                            ->select([
                                    "$e.ParentID",
                                    "$p.ID as PersonID",
                                    "$pas.[LastName]+' '+$pas.[FirstName]+' '+$pas.[MiddleName] as Fio",
                                    "$c.Email",
                                    "$c.URL as Url",
                                    "$c.WorkTelephone",
                                    "$pt.Name as Post",
                                    "$sp.DepartmentParentID",
                                    "isnull($c.InDirectory,0) as InDirectory",
                                    "$c.OrderPosition",
                                ])
                            ->distinct()
                            ->joinWith(['person','person.passport','person.contact','staffPosition','staffPosition.job','staffPosition.department','staffPosition.job.post','employeeState'],false)
                            ->andWhere([ "$es.Label"=>'valid' ])
                            ->andWhere([ 'in',"$e.IsMain",[1,-1] ])
                            ->andWhere("$d.TreeDepartment like '$TreeDepartment%'")
                            ->orderBy("$c.OrderPosition,$pas.[LastName]+' '+$pas.[FirstName]+' '+$pas.[MiddleName]")
                            ->asArray()->all();
    }
    
    public function actionCalcvacation($employeeparentid, $startdate, $enddate, $vacationtypeid, $days)
    {
        \Yii::$app->response->format = 'json';
        $e = Employee::find()->andWhere(['ParentID' => $employeeparentid, 'IsRaw' => 0])->one();        
        return $e->calcVacationPeriods($startdate, $enddate, $vacationtypeid, $days);
    }
    
    public function actionSuggestemployee($term) {
        if(strlen($term) > 1) {
			\Yii::$app->response->format = 'json';
			
			$pAlias = \common\modules\persons\models\Passport::tableName();
			$query = Employee::find()
					->joinWith(['person.passport'], false)
                                        ->andWhere(['in','IsMain',[-1,1]])
					->limit(Yii::$app->params['maxSuggest']);
			
			foreach(explode(' ', $term) as $value) {
				$query->andWhere(["LIKE", "{$pAlias}.LastName + ' ' + {$pAlias}.FirstName + ' ' + {$pAlias}.MiddleName + ' ' + CAST({$pAlias}.BirthDate as varchar)", trim($value)]);
			}
			
			return \common\components\Helper::objectToArray($query->all(), ['ParentID' => 'id', 'PersonHR' => 'label', 'PersonHR' => 'value']);
		} else {
			throw new NotFoundHttpException('The requested page does not exist.');
		}
    }
    
    public function actionReload($parentID)
    {
        $model = Employee::find()->andWhere(['ParentID' => $parentID, 'IsRaw' => 0, 'IsActual' => 1])->one();
        if (!is_null($model)){
            $StudYear = Date('Y')-(Date('m')<9 ? 1 : 0);            
            \common\modules\ok\models\EmployeePlanLoad::reload($StudYear, $model);            
        }
        return $this->redirect(['index', 'parentid' => $parentID]);
    }
}
