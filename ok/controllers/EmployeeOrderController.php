<?php

namespace common\modules\ok\controllers;

use Yii;
use common\modules\ok\models\EmployeeOrder;
use common\modules\ok\models\searches\EmployeeOrderSearch;
use common\components\Controller;
use yii\web\NotFoundHttpException;
use common\modules\roles\models\ACLRole;
use common\modules\ok\models\Employee;
use common\modules\ok\models\Vacation;
use common\modules\ok\models\VacationPeriod;
use common\modules\ok\models\VacationMove;
use common\modules\ok\models\VacationPlan;
use common\modules\ok\models\Attestation;
use common\modules\orders\models\searches\EmployeeGeneralOrderSearch;
use common\modules\ok\models\EmployeeHarmLink;

/**
 * EmployeeOrderController implements the CRUD actions for EmployeeOrder model.
 */
class EmployeeOrderController extends Controller
{
    public function behaviors()
    {
        return ACLRole::defaultBehaviors();
    }

    /**
     * Lists all EmployeeOrder models.
     * @return mixed
     */
    public function actionIndex($processed = 0, $orderID = '')
    {
        $searchModel = new EmployeeOrderSearch();
        $searchModel->populateRelation('employee', new Employee);
        $searchModel->GeneralOrderID = -1;
        $dataProvider = $searchModel->search($processed > 0 ? [] : Yii::$app->request->queryParams);
        
        $searchModelGO = new EmployeeGeneralOrderSearch();
        $searchModelGO->EntryDate = -1;
        $dataProviderGO = $searchModelGO->search(Yii::$app->request->queryParams);
        
      /*  $searchModelClosed = new EmployeeOrderSearch();
        $searchModelClosed->populateRelation('employee', new Employee);
        $dataProviderClosed = $searchModelClosed->search($processed > 0 ? Yii::$app->request->queryParams : [], true);*/

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            //'searchModelClosed' => $searchModelClosed,
            //'dataProviderClosed' => $dataProviderClosed,
            'searchModelGO' => $searchModelGO,
            'dataProviderGO' => $dataProviderGO,
            'order' => empty($orderID) ? [] : explode(',', $orderID),
        ]);
    }

    /**
     * Displays a single EmployeeOrder model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new EmployeeOrder model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($personID = 0, $employeeParentID = 0, $staffPositionParentID = 0)
    {
        $model = new EmployeeOrder();
        if ($employeeParentID > 0) {
            $employee = Employee::find()->andWhere(['ParentID' => $employeeParentID])->one();
        }
        if (!isset($employee) ||is_null($employee)){
            $employee = new Employee;
            $employee->WorkDay = '5';
            $employee->WorkHour = '8';
            $employee->VacationDay = 28;
            $employee->VacationDayDop = 0;
            if ($personID > 0) {
                $employee->PersonID = $personID;
               // $employee->populateRelation('person',);
            }
            if ($staffPositionParentID > 0) $employee->StaffPositionParentID = $staffPositionParentID;           
        }
        $model->populateRelation('employee', $employee);
        $model->employee->ok = true;
        $model->employee->ChangeDate = null;
        $model->employee->JobTypeID = 1;
        $model->employee->Other = null;
        $model->employee->DocumentDate = null;
       
        $model->populateRelation('vacation', new Vacation);
        $model->vacation->populateRelation('vacationPeriods', [new VacationPeriod,new VacationPeriod,new VacationPeriod,new VacationPeriod]);
        $model->populateRelation('vacationMove', new VacationMove);
        $model->populateRelation('vacationPlan', new VacationPlan);
        $model->populateRelation('attestation', new Attestation);
        $model->populateRelation('employeeHarmLinks', [new EmployeeHarmLink,new EmployeeHarmLink,new EmployeeHarmLink,new EmployeeHarmLink]);
 
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->ID]);
        } else {
            return $this->render('create', [
                'model' => $model, 'staffPositionParentID' => $staffPositionParentID
            ]);
        }
    }

    /**
     * Updates an existing EmployeeOrder model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        if ($model->employee)
        $model->employee->ok = true;
        
        if ($model->vacation){
            $model->vacation->DateStart = $model->vacation->vacationInfo->DateStart;
            $model->vacation->DateEnd = $model->vacation->vacationInfo->DateEnd;
            $model->vacation->VacationDays = $model->vacation->vacationInfo->VacationDays;
        }
        
        if (!$model->vacationPlan && in_array($model->EmployeeOrderTypeID, [12])){
            $model->populateRelation('vacationPlan', new VacationPlan);
            $model->vacationPlan->isSet=0;
        }
        
        if (count($model->employeeHarmLinks) < 4 && in_array($model->EmployeeOrderTypeID, [1,2,4])){
            $harms = $model->employeeHarmLinks;
            for($i = count($harms);$i < 4;$i++){
                $harms[] = new EmployeeHarmLink;
            }
            $model->populateRelation('employeeHarmLinks', $harms);
        }
        
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->ID]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing EmployeeOrder model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->deleteOrder();

        return $this->redirect(['index']);
    }

    /**
     * Finds the EmployeeOrder model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return EmployeeOrder the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = EmployeeOrder::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    public function actionFilters()
    {
        $params = Yii::$app->request->post();
        if (!isset($params['UserID']) || empty($params['UserID'])) $params['UserID'] = Yii::$app->user->id;
        $moduleOk = Yii::$app->getModule('ok');
        $key = $moduleOk->keyPrefix.Yii::$app->user->id;
        
        return Yii::$app->cache->set($key, $params, $moduleOk->cacheTime);
    }
}
