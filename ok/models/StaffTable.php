<?php

namespace common\modules\ok\models;

use Yii;
use common\modules\department\models\Department;
use yii\db\Query;
use yii\web\ServerErrorHttpException;
use common\modules\roles\models\UserDepartment;
use common\modules\ok\Module;

class StaffTable {
    
    public static function getDepartmentTree($userID = 0)
    {
        if ($userID == 0) $userID = Yii::$app->user->id;
            $userDepartments = UserDepartment::find()->where(['UserID' => $userID])->all();
        $ds = [];  $path = []; $cond = []; $condition = ''; 
          
        if (count($userDepartments) > 0){
            foreach ($userDepartments as $userDepartment){
            $ds[] = $userDepartment->DepartmentID;
            $path = array_merge($path, explode('::',$userDepartment->department['TreeDepartment']));
            $cond[] = "TreeDepartment like '%{$userDepartment->DepartmentID}%'";
            }  
           $condition = implode(' OR ',$cond).' OR ParentID in ('.implode(',',$path).')'; 
        } else {$ds[] = 100001; $path = [100001];}
       
        $departments = Department::find()->select(['ID','Name as label','DepartmentID','ParentID','TreeDepartment'])
                ->andWhere($condition)->asArray()->all();
        $listIdParent = [];
        foreach ($departments as $department)
            $listIdParent[$department['ParentID']]=$department;

        $rootId = 0;

          foreach ($listIdParent as $id => $node) {
            if ($node['DepartmentID']) {
                    $listIdParent[$node['DepartmentID']]['children'][$id] =& $listIdParent[$id];
            } else {

              $rootId = $id;
            }
          }

        $tree = [$rootId => $listIdParent[$rootId]];
        
        return ['result'=>$tree];
    }
    
    public static function getPositions($departmentParentID, $userID = 0)
    {
        if ($userID == 0) $userID = Yii::$app->user->id;
            $userDepartments = UserDepartment::find()->where(['UserID' => $userID])->all();
            if (count($userDepartments) > 0){
            $ds = [];
            foreach ($userDepartments as $userDepartment){        
                $ds[] = $userDepartment->DepartmentID;        
                }
            $dep = Department::find()->andWhere(['ParentID' => $departmentParentID])->one(); 
            if (!count(array_intersect(explode('::',$dep->TreeDepartment), $ds)))
                    return ['' => ''];
        }
        
        $query = new Query;
        $query->select(['p.Name as Post', 
                        'p.ID as PostID',
                        'rl.Label as Rate', 
                        'r.ParentID as RateID',
                        'sc.Name as StaffCategory',
                        'sc.ID as StaffCategoryID',
                        'ft.Name as FinanceType',
                        'ft.ID as FinanceTypeID',
                        "iif(isnull(pas.LastName,'')='','',pas.LastName)+' '+iif(isnull(pas.FirstName,'')='','',pas.FirstName)+' '+"
                      . "iif(isnull(pas.MiddleName,'')='','',pas.MiddleName) as Person",
                        'sp.ID as ID',
                        'iif(sp.ID > 0,0,0) as Selected',
                        'isnull(e.PersonID,0) as PersonID',
                        'isnull(e.ParentID,0) as EmployeeParentID',
                        'isnull(e.Value,0) as PersonValue',
                        'sp.Value as Value',
                        "isnull(convert(varchar,sp.StartDate,104),'') as StartDate",
                        "isnull(si.Number,'') as InstructionNumber",
                        "isnull(convert(varchar,si.Date,104),'') as InstructionDate",
                       // "iif(isnull(si.Number,'')='','',isnull(si.Number,'')+' '+'".Module::t('ML','from')."'+' '+isnull(convert(varchar,si.Date,104),'')) as Instruction",
                        'si.ID as InstructionID',
                        'sp.IsRaw as Modified',
                        'sp.ParentID as ParentID',
                        'isnull(sp.StaffOrderID,0) as StaffOrderID',
                        'isnull(spchild.ID,0) as InProcess',
                        'isnull(spparent.ID,0) as ActualParentID',
                        'so.GeneralOrderID as GeneralOrderID',
                        'so.StatusDel as StatusDel',
                        'isnull(spi.ID,0) as IncreaseID',
                        'isnull(spi.IncreaseTypeID,0) as IncreaseTypeID',
                        'isnull(spi.Sum,0) as Sum',
                        'isnull(spi.BaseRateMultiplier,0) as BaseRateMultiplier',
                        "isnull(sp.AlterPostName,'') as AlterPostName",
                        "iif(echild.ID>0,echild.IsRaw,e.IsRaw) as EmployeeRaw",          
                        "iif(isnull(echild.ID,0)=0,isnull(e.EmployeeOrderID,0),echild.EmployeeOrderID) as EmployeeOrderID",
                        'isnull(echild.ID,0) as EmpInProcess',
                        'iif(echild.ID>0 and eoch.EmployeeOrderTypeID in (2,3),1,0) as EmpExit',
                        'isnull(e.EmployeeStateID,0) as EmployeeStateID',
                        'e.IsRaw as Raw',
                        'isnull(spparent.Value,0) as ParentValue',
                        'isnull(eparent.Value,0) as BeforeVacationValue',
                        ])
                ->from('tbl_staff_position sp')
                ->leftJoin('tbl_staff_category sc','sc.ID = sp.StaffCategoryID')
                ->leftJoin('tbl_finanse_type ft','ft.ID = sp.FinanceTypeID')
                ->leftJoin('tbl_job j','j.ParentID = sp.JobParentID and j.IsActual = 1')
                ->leftJoin('tbl_post_new p','p.ID = j.PostID')
                ->leftJoin('tbl_rate r','r.ParentID = j.RateParentID and r.IsActual = 1')
                ->leftJoin('tbl_rate_label rl','rl.ID = r.RateLabelID')
                ->leftJoin('tbl_employee_new e','e.StaffPositionParentID = sp.ParentID and e.IsActual = 1 and sp.IsRaw = 0 and e.EmployeeStateID <> 100390')
                ->leftJoin('tbl_passport pas','pas.PersonID = e.PersonID and pas.IsActual = 1')
                ->leftJoin('tbl_staff_instruction si','si.ID = sp.StaffInstructionID')
                ->leftJoin('tbl_staff_position spchild','sp.ParentID = spchild.ParentID and spchild.IsRaw = 1 and spchild.IsActual = 1 and spchild.ID > sp.ID')
                ->leftJoin('tbl_staff_position spparent','sp.ParentID = spparent.ParentID and spparent.IsRaw = 0 and spparent.IsActual = 1 and spparent.ID < sp.ID')
                ->leftJoin('tbl_staff_order so','so.ID = sp.StaffOrderID')
                ->leftJoin('tbl_staffposition_increase spi','spi.StaffPositionID = sp.ID')                
                ->leftJoin('tbl_employee_new echild','e.ParentID = echild.ParentID and echild.IsRaw = 1 and echild.IsActual = 1 and echild.ID > e.ID')
                ->leftJoin('tbl_employee_new eparent', 'eparent.ID = (select top 1 ID from tbl_employee_new where ParentID = e.ParentID and IsRaw = 0 and IsActual = 0 and EmployeeStateID = 100391 order by ID desc) and e.EmployeeStateID = 100392')
                ->leftJoin('tbl_employee_order eoch','echild.EmployeeOrderID = eoch.ID')
                ->where(['sp.DepartmentParentID' => $departmentParentID, 'sp.IsActual' => 1])
                ->orderBy('sp.ParentID, sp.ID, pas.LastName, pas.FirstName, e.ID');        
        $command = $query->createCommand();     
        $result = $command->queryAll();
        $output = []; $persons = []; $increases = [];
        
        foreach ($result as $res){
            $letterOrd = '';  
            $res['Value'] = round($res['Value'],5);
            if (!$res['Modified']){
            $p = [];
            if ($res['PersonID'] > 0){
                $p['Person'] = $res['Person'];
                $p['PersonID'] = $res['PersonID'];
                $p['PersonValue'] = round($res['PersonValue'],5);
                $p['EmployeeParentID'] = $res['EmployeeParentID'];
                $p['EmployeeRaw'] = $res['EmployeeRaw'];
                $p['EmployeeOrderID'] = $res['EmployeeOrderID'];
                $p['EmpInProcess'] = $res['EmpInProcess'];
                $p['EmpExit'] = $res['EmpExit'];
                $p['EmployeeStateID'] = $res['EmployeeStateID'];
                $p['Raw']=$res['Raw'];
                $p['BeforeVacationValue'] = round($res['BeforeVacationValue'],5);
            }
            unset($res['Person']); unset($res['PersonID']); unset($res['PersonValue']); unset($res['BeforeVacationValue']);
            $spi = [];
            if ($res['IncreaseTypeID'] > 0){
                $spi['IncreaseTypeID'] = $res['IncreaseTypeID'];
                $spi['Sum'] = $res['Sum'];
                $spi['BaseRateMultiplier'] = $res['BaseRateMultiplier'];
            }
            unset($res['IncreaseTypeID']); unset($res['Sum']); unset($res['BaseRateMultiplier']);
            
            if (!isset($output[$letterOrd.'_'.$res['ParentID'].'_'.$res['ID']])){
                $output[$letterOrd.'_'.$res['ParentID'].'_'.$res['ID']] = $res;                
                //@$output['Value'] += $res['Value'];
            }
            //if (count($p)){
                if (!isset($persons[$res['EmployeeParentID'].'_'.$res['ParentID']])){
                    $output[$letterOrd.'_'.$res['ParentID'].'_'.$res['ID']]['Persons'][] = $p;
                    $persons[$res['EmployeeParentID'].'_'.$res['ParentID']] = 1;
                    if( isset($p['Raw']) && $p['Raw']==0){
                    @$output[$letterOrd.'_'.$res['ParentID'].'_'.$res['ID']]['Hold'] += $p['PersonValue']; 
                    if ($p['EmployeeStateID'] != 100391)
                        @$output[$letterOrd.'_'.$res['ParentID'].'_'.$res['ID']]['HoldWOAbsent'] += $p['PersonValue'];
                    @$output[$letterOrd.'_'.$res['ParentID'].'_'.$res['ID']]['HoldWithAbsent'] += ($p['EmployeeStateID'] == 100392) ? $p['BeforeVacationValue'] : $p['PersonValue'];
                    //@$output['Hold'] += $p['PersonValue'];
                   
                    }
                }
                if(count($spi)==0) $spi = ['' => ''];
                if (!isset($increases[$res['IncreaseID'].'_'.$res['ParentID']])){
                    $output[$letterOrd.'_'.$res['ParentID'].'_'.$res['ID']]['Increases'][] = $spi;
                    $increases[$res['IncreaseID'].'_'.$res['ParentID']] = 1;}
            //}
            }else{
                $spi = [];
            if ($res['IncreaseTypeID'] > 0){
                $spi['IncreaseTypeID'] = $res['IncreaseTypeID'];
                $spi['Sum'] = $res['Sum'];
                $spi['BaseRateMultiplier'] = $res['BaseRateMultiplier'];
            }
            unset($res['IncreaseTypeID']); unset($res['Sum']); unset($res['BaseRateMultiplier']);
            
                if (!isset($output[$res['ParentID'].'_'.$res['ID']])){
                $output[$letterOrd.'_'.$res['ParentID'].'_'.$res['ID']] = $res;               
                $output[$letterOrd.'_'.$res['ParentID'].'_'.$res['ID']]['Persons'][] = [];
                }
                if(count($spi)==0) $spi = ['' => ''];
                if (!isset($increases[$res['IncreaseID'].'_'.$res['ParentID']])){
                    $output[$letterOrd.'_'.$res['ParentID'].'_'.$res['ID']]['Increases'][] = $spi;
                    $increases[$res['IncreaseID'].'_'.$res['ParentID']] = 1;}
            }
            
        }
        if (!count($output)) $output=[''=>''];
        return $output;
    }
    
    
    public static function savePosition($attributes)
    {
        if (isset($attributes['spID']))
            $sp = StaffPosition::find()->andWhere(['ID' => $attributes['spID']])->orderBy('ID desc')->one(); 
        if (!isset($sp) || is_null($sp)){
            $sp = new StaffPosition;
        }
        if (isset($attributes['Instruction']) || isset($attributes['InstructionID'])) return self::saveInstruction($attributes,$sp);
        
        $changeIncreases = $sp->isChangedIncreases(isset($attributes['Increases']) ? $attributes['Increases'] : []);
        $fl = true;
        $changePosition = isset($attributes['StaffPosition']) || isset($attributes['Job']) || isset($attributes['delete']) || $changeIncreases;
        $old = $sp->IsRaw == 1 ? false : true;
        
        if ($old) {
            $sp->IsRaw = (($sp->IsRaw==0 && $changePosition) || !isset($attributes['spID'])) ? 1 : 0;
        }
        
        $transaction = Yii::$app->db->beginTransaction();
        try{
            
            if (isset($attributes['Job'])) {
                $jobAttr = $sp->job ? $sp->job : new Job;
                $jobAttr->attributes = $attributes['Job'];
                if(isset($attributes['Job']['RateID'])) $jobAttr->RateParentID = $attributes['Job']['RateID'];
            
                $job = Job::find()->andWhere(['PostID' => $jobAttr->PostID, 'RateParentID' => $jobAttr->RateParentID])->one();
                if (!is_null($job)){
                    $sp->JobParentID = $job->ParentID;
                    $sp->populateRelation('job',$job);
                }
                else {throw new ServerErrorHttpException(Module::t('ML','Сочетания должности и разряда не существует.'));}
            } 
            if (isset($attributes['StaffPosition'])){
                $sp->attributes = $attributes['StaffPosition'];
            }
            
            if ((!$sp->staffOrder || $old) && $changePosition){
            $so = new StaffOrder;           
            if (isset($attributes['delete'])) $so->StatusDel = 1;
            $so->populateRelation('staffPosition',$sp);
            $fl = $so->save() && $fl;
            $sp->StaffOrderID = $so->ID;
            $sp->populateRelation('staffOrder',$so);
            }
            
            $sp->loadIncreases(isset($attributes['Increases']) ? $attributes['Increases'] : []);    
            $fl = $sp->save() && $fl;
           
            if ($fl) $transaction->commit();
                else {$transaction->rollBack();
                $spie = [];
                foreach ($sp->staffPositionIncreases as $incr) $spie = array_merge($incr->firstErrors, $spie);
                $errs = array_merge($sp->firstErrors,$spie);
                throw new ServerErrorHttpException(Module::t('ML',implode(',',$errs).' '.implode(',',array_keys($errs))));
                }   
            }
        catch(Exception $e) {
			$transaction->rollBack();
                        Yii::$app->response->setStatusCode(500);
                        return ['message'=>$e->getMessage()];
			//throw $e;
		}
        catch(ServerErrorHttpException $e){
            $transaction->rollBack();
            Yii::$app->response->setStatusCode(500);
                        return ['message'=>$e->getMessage()];
        }        
        
       return $fl; 
    }
    
    public static function deletePosition($id)
    {
        $sp = StaffPosition::find()->andWhere(['ID' => $id])->one();
        $fl = true;
        $transaction = Yii::$app->db->beginTransaction();
        try {
            if ($sp->IsRaw == 1){
                $sp->staffOrder->delete();
                $sp->delete();
            }
            else {
                $fl = self::savePosition(['spID' => $id, 'delete' => true]);
            }
            if ($fl) $transaction->commit();
                else $transaction->rollBack();
                           
        }  
        catch(Exception $e) {
			$transaction->rollBack();
			throw $e;
		}
        catch(ServerErrorHttpException $e){
            $transaction->rollBack();
            Yii::$app->response->setStatusCode(500);
                        return ['message'=>$e->getMessage()];
        }          
    } 
    
    public static function saveInstruction($attributes, $sp)
    {
        $fl = true;     
        $sp->IsRaw = 0;        
        
        $transaction = Yii::$app->db->beginTransaction();
        try{
            if (isset($attributes['Instruction'])){
               if (isset($attributes['Instruction']['delete'])){
                   $sp->StaffInstructionID = null;
               }
               elseif (isset($attributes['Instruction']['Number']) && trim($attributes['Instruction']['Number'])){  
               
                $si = StaffInstruction::find()->andWhere(['Number' => $attributes['Instruction']['Number']])->andWhere("convert(varchar,Date,104) = '".date('d.m.Y', strtotime($attributes['Instruction']['Date']))."'")->one();
                if (is_null($si)){
                    $si = new StaffInstruction;
                    $si->attributes = $attributes['Instruction'];  
                    $si->Date = date('d.m.Y', strtotime($si->Date));
                    $fl = $fl && $si->save();
                }
            $sp->StaffInstructionID = $si->ID; }
            else $fl= false;
            }
            elseif (isset($attributes['InstructionID'])){
                $sp->StaffInstructionID = $attributes['InstructionID'];
            }   
            if ($fl)
                StaffPosition::updateAll(['StaffInstructionID' => $sp->StaffInstructionID],['ParentID' => $sp->ParentID, 'IsActual' => 1]);
           
            if ($fl) $transaction->commit();
                else {$transaction->rollBack();
                
                throw new ServerErrorHttpException(Module::t('ML','Проверьте номер/дату'));
                }   
            }
        catch(Exception $e) {
			$transaction->rollBack();
                        Yii::$app->response->setStatusCode(500);
                        return ['message'=>$e->getMessage()];
			//throw $e;
		}
        catch(ServerErrorHttpException $e){
            $transaction->rollBack();
            Yii::$app->response->setStatusCode(500);
                        return ['message'=>$e->getMessage()];
        }        
        
       return $fl; 
    }
}
