<?php

namespace common\modules\ok\models;

use Yii;
use common\modules\orders\models\EmployeeGeneralOrder;
use common\modules\persons\models\PassportProcess;
use common\modules\persons\models\Passport;
use common\modules\department\models\Department;
use common\modules\persons\models\PtLink;
use common\modules\ok\Module;
use common\components\Helper;


/**
 * This is the model class for table "{{%employee_order}}".
 *
 * @property integer $ID
 * @property integer $GeneralOrderID
 * @property string $FormDate
 * @property string $Date
 * @property integer EmployeeOrderTypeID
 */
class EmployeeOrder extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%employee_order}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['GeneralOrderID', 'EmployeeOrderTypeID'], 'integer'],
            [['FormDate', 'EmployeeOrderTypeID'], 'required'],
            [['FormDate', 'Date'], 'safe'],
            [['Number'], 'safe'],
            [['EmployeeOrderTypeID'], 'validateSecondOrder'],
            [['EmployeeOrderTypeID'], 'validateVacationPeriod']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => Module::t('ML', 'ID'),
            'GeneralOrderID' => Module::t('ML', 'General Order'),
            'FormDate' => Module::t('ML', 'Form Date'),
            'Date' => Module::t('ML', 'Date'),
            'EmployeeOrderTypeID' => Module::t('ML', 'Order Type'),
            'personHR' => Module::t('ML', 'Person'),
            'financeTypeName' => Module::t('ML', 'Finance Type'),
            'staffCategoryName' => Module::t('ML', 'Category'),
            'mainDepartmentName' => Module::t('ML', 'Department'),
            'userName' => Module::t('ML', 'User'),
            'Number' => Module::t('ML', "Number"),
        ];
    }
    
    public function validateSecondOrder($attribute, $params)
    {
        if ($this->isNewRecord && $this->employee && $this->employee->ParentID > 0){
            $e = Employee::find()->andWhere(['ParentID' => $this->employee->ParentID, 'IsRaw' => 1])->one();
            if (!is_null($e)) 
                $this->addError($attribute, 'Существует непроведенный приказ по сотруднику');
        }
      }
      
    public function validateVacationPeriod($attribute, $params)
    {
        if (in_array($this->EmployeeOrderTypeID,[5]) && $this->vacation){ 
                    if (count($this->vacation->vacationPeriods) == 0) $this->addError($attribute, 'Проверьте даты отпуска и количество дней');
                    $k = 0;
                    foreach ($this->vacation->vacationPeriods as $vp){                       
                       if ($vp->VacationDays > 0) {$k++; break;}
                    }
                    if ($k == 0) $this->addError($attribute, 'Проверьте даты отпуска и количество дней');
                }
      } 
    
    /**
	 * @return \yii\db\ActiveQuery 
	 */
    public function getEmployee() {
		return $this->hasOne(Employee::className(), ['EmployeeOrderID' => 'ID']);
	}
        
    public function getEmployeeAll() {
		return $this->hasOne(Employee::className(), ['EmployeeOrderID' => 'ID'])->where(Employee::tableName().'.IsActual > -1');
	}
            
    public function getEmployeeLast() 
    {
	return $this->hasOne(Employee::className(), ['EmployeeOrderID' => 'ID'])->where(Employee::tableName().'.IsActual > -1')
                ->orderBy(Employee::tableName().'.VersionDate DESC');
    }    
        
    /**
	 * @return \yii\db\ActiveQuery 
	 */
    public function getEmployeeOrderType() {
		return $this->hasOne(EmployeeOrderType::className(), ['ID' => 'EmployeeOrderTypeID']);
	}    
        
    public function getEmployeeHarmLinks() 
    {
        return $this->hasMany(EmployeeHarmLink::className(), ['EmployeeOrderID' => 'ID']);
    }     
        
    public function insert($runValidation = true, $attributes = null) 
    {
        $this->FormDate = date('d.m.Y H:i:s');       
        return parent::insert($runValidation, $attributes);
    }
    
    public function load($data, $formName = null) 
    {
        parent::load($data, $formName); 
        if (isset($data['PassportProcess'])){
            if (!$this->employee->passportProcess){
                $this->employee->populateRelation('passportProcess', new PassportProcess);
                $this->employee->passportProcess->attributes = $this->employee->passport->attributes;
            } 
            $this->employee->passportProcess->load($data,'PassportProcess');
            $this->employee->passportProcess->VersionDate = date('d.m.Y H:i:s');
            
            $this->employee->passportProcess->BirthDate = Helper::CheckDateTimeMatch($this->employee->passport->BirthDate);
            $this->employee->passportProcess->PasDate = Helper::CheckDateTimeMatch($this->employee->passport->PasDate);
            $this->employee->passportProcess->ArrivalDate = Helper::CheckDateTimeMatch($this->employee->passport->ArrivalDate);
          
        }
        if (isset($data['Vacation'])){
            if (!$this->vacation){
                $this->populateRelation('vacation', new Vacation);}
            $this->vacation->load($data,'Vacation');
        
            if (isset($data['VacationPeriod'])){
                $t = 0;
                $periods = $this->vacation->vacationPeriods;
                foreach($periods as $p) $p->VacationDays = 0;
                foreach($data['VacationPeriod'] as $key => $vp){
                    if (!isset($periods[$key])) $periods[$key] = new VacationPeriod;
                    $periods[$key]->attributes = $vp;
                    if ($periods[$key]->VacationDays > 0) $t++;
                }
                if ($t==0){
                   if(isset($this->vacation->vacationPeriods[0])) {$p = $this->vacation->vacationPeriods[0];}
                        else {$p = new VacationPeriod;}
			$p->VacationDays  = $data['Vacation']['VacationDays'];
			$p->DateStart  = $data['Vacation']['DateStart'];
			$p->DateEnd  = $data['Vacation']['DateEnd'];		
                        $periods = [$p];
                }
                $this->vacation->populateRelation('vacationPeriods', $periods);
            }
                        
        } 
        if (isset($data['VacationMove'])){
            if (!$this->vacationMove){
                $this->populateRelation('vacationMove', new VacationMove);}
            $this->vacationMove->load($data,'VacationMove');
        }
        
        if (isset($data['VacationPlan'])){
            if (!$this->vacationPlan){
                $this->populateRelation('vacationPlan', new VacationPlan);}
            $this->vacationPlan->load($data,'VacationPlan');            
            $this->vacationPlan->PersonID = $this->employee->PersonID;
            $this->vacationPlan->Year = date('Y', strtotime($this->vacationPlan->DateStart));
            $this->vacationPlan->VacationTypeID = 1;
        }
        
        if (isset($data['Attestation'])){
            if (!$this->attestation){
                $this->populateRelation('attestation', new Attestation);}
            $this->attestation->load($data,'Attestation');
        }
        
        if (isset($data['EmployeeHarmLink'])) {
            $harms = $this->employeeHarmLinks;
            foreach ($harms as $harm){
                $harm->HarmTypeID = 0;
            }
            foreach ($data['EmployeeHarmLink'] as $key => $item){
                if (!isset($harms[$key])) $harms[$key] = new EmployeeHarmLink;                
                $harms[$key]->attributes = $item;
            }
            $this->populateRelation('employeeHarmLinks', $harms);            
        };
    
        return $this->employee->load($data, 'Employee');
    }
    
    public function save($runValidation = true, $attributeNames = null) 
    { 
        $needTransaction = Yii::$app->db->transaction;
        if(empty($needTransaction))
        $transaction = Yii::$app->db->beginTransaction();
            try {  
                if (empty($this->Number)) {$this->Number = $this->numberGenerate();}
                $fl = parent::save($runValidation, $attributeNames);
                            
                $this->employee->IsRaw = 1;
                
                switch ($this->employee->EmployeeStateID){                   
                    case 100391 : //Временно отсутствует
                        $this->employee->EmployeeStateID = $this->employeeOrderType->EmployeeStateAfterVacationID;
                        break;
                    
                    case 100392 : //Работает в отпуске
                        $this->employee->EmployeeStateID = $this->employeeOrderType->EmployeeStateAfterAllowID;
                        break;
                    
                    default : 
                        $this->employee->EmployeeStateID = 
                        in_array($this->EmployeeOrderTypeID,[5]) && !empty($this->vacation->vacationType->EmployeeStateID) ? 
                        $this->vacation->vacationType->EmployeeStateID : $this->employeeOrderType->EmployeeStateID;
                }
                
                $this->employee->EmployeeOrderID = $this->ID;
                
                if (in_array($this->EmployeeOrderTypeID,[5])){ 
                    $this->vacation->EmployeeOrderID = $this->ID;
                    $fl = $fl && $this->vacation->save();  
                    if (count($this->vacation->vacationPeriods) == 0) $fl = false;
                    $k = 0;
                    foreach ($this->vacation->vacationPeriods as $vp){                       
                       if ($vp->VacationDays > 0) {
                           $vp->VacationID = $this->vacation->ID;
                           $fl = $fl && $vp->save(); $k++;}
                       elseif ($vp->ID > 0) $vp->delete(); 
                    }
                    if ($k == 0) $fl = false;
                }
                
                if (in_array($this->EmployeeOrderTypeID,[9,10,12,15])){      
                    $this->vacationMove->EmployeeOrderID = $this->ID;
                    $fl = $fl && $this->vacationMove->save(); 
                }
                
                if (in_array($this->EmployeeOrderTypeID,[6])){      
                    $this->vacationPlan->EmployeeOrderID = $this->ID;
                    $fl = $fl && $this->vacationPlan->save(); 
                }
                
                if (in_array($this->EmployeeOrderTypeID,[12]) && $this->vacationPlan){   
                    if ($this->vacationPlan->isSet > 0){
                    $this->vacationPlan->EmployeeOrderID = $this->ID;
                    $fl = $fl && $this->vacationPlan->save(); }
                    elseif ($this->vacationPlan->ID > 0) $this->vacationPlan->delete();
                }
                
                if (in_array($this->EmployeeOrderTypeID,[8])){      
                    $this->attestation->EmployeeOrderID = $this->ID;
                    $fl = $fl && $this->attestation->save(); 
                }
                
                if ($this->EmployeeOrderTypeID == 3)
                    $this->employee->EndDate = $this->employee->ChangeDate;
                
                if ($this->EmployeeOrderTypeID == 1 && empty($this->employee->ContractNumber)){
                    $this->employee->ContractNumber = $this->employee->contractNumberGeneration();
                }
                
                $fl = $fl && $this->employee->save(); 
                
                if ($this->EmployeeOrderTypeID == 7){
                    $this->employee->passportProcess->OrderID = $this->ID;
                    $fl = $fl && $this->employee->passportProcess->save();
                }
                
                if (in_array($this->EmployeeOrderTypeID,[1,2,4])){
                    foreach ($this->employeeHarmLinks as $harm){
                        if ($harm->HarmTypeID > 0){
                            $harm->EmployeeOrderID = $this->ID;
                            $fl = $fl && $harm->save();
                        } elseif ($harm->ID > 0) $harm->delete();
                    }
                }
                
                if(empty($needTransaction)) {
                    if ($fl) $transaction->commit();
                    else  $transaction->rollback();}
                return $fl;
            }
            catch(Exception $e) 
            {
                if(empty($needTransaction))
                    $transaction->rollback();
                throw $e;
                return false;
            }
    }
    
    public function getPersonHR()
    {
        return $this->employeeAll && $this->employeeAll->passport ? $this->employeeAll->passport->personHR : '';
    }
    
    public function isClosed()
    {
        return $this->generalOrder ? $this->generalOrder->isClosed() : (!$this->employeeAll->IsRaw);
    }
    
    public function getGeneralOrder() 
    {
	return $this->hasOne(EmployeeGeneralOrder::className(), ['ID' => 'GeneralOrderID']);
    }
    
    public function process()
    {
        if ($this->employee->IsRaw == 1){
            $rawID = $this->employee->ID;
            $e = $this->employee;
           
                $e->IsRaw = 0;               
                $e->save(false);
                
            if ($this->EmployeeOrderTypeID == 1 && empty($this->employee->person->PersonCodeID)){
                $this->employee->setPersonCode();
            } 
            if ($this->EmployeeOrderTypeID == 1 && !PtLink::find()->andWhere(['PersonID'=>$this->employee->PersonID, 'PersonTypeID'=>2])->one() ){
                $this->employee->setPtlink();
            } 
            if ($this->EmployeeOrderTypeID == 3 && Employee::find()->joinWith(['employeeState'],false)->andWhere(['PersonID'=>$this->employee->PersonID,'IsRaw'=>0,"Label"=>'valid'])->count() == 0 ){
                $pt = PtLink::findOne(['PersonID'=>$this->employee->PersonID, 'PersonTypeID'=>2]);
                if ($pt) $pt->delete();               
            } 
           
            if ($this->employee->passportProcess && $this->EmployeeOrderTypeID == 7){
                Passport::updateAll (['IsActual' => 0], ['IsActual' => 1, 'PersonID' => $this->employee->PersonID]);
                $this->employee->passportProcess->IsActual = 1;
                $this->employee->passportProcess->save();
            }
             
            if ($this->vacationMove && in_array($this->EmployeeOrderTypeID, [9,10,12,15])){
                $this->vacationMove->vacation->Moved = 1;
                //учитываем все переносы
                $vms = VacationMove::find()->andWhere(['VacationID' => $this->vacationMove->VacationID])->all();
                $sumdays = 0;
                foreach($vms as $vm){
                    $sumdays += $vm->getDaysCount();
                }
                
                $this->vacationMove->vacation->FactDays = $this->vacationMove->vacation->vacationDaysCount - $sumdays;
                $this->vacationMove->vacation->save();       
                
              //  if ($this->vacationMove && in_array($this->EmployeeOrderTypeID, [9,10,12])){ 
                    
                $factDays = $this->vacationMove->vacation->FactDays;    
                //закрываем периоды последовательно
                foreach ($this->vacationMove->vacation->vacationPeriods as $vp){
                  if ($factDays >= 0) {
                    if ($vp->VacationDays - $factDays > 0){
                        $vp->Moved = 1;
                        $vp->FactDays = $factDays;
                        $vp->save();  
                        $factDays -= $vp->FactDays;
                    } else {
                        $factDays -= $vp->VacationDays;
                    } 
                  }
                    /*
                    //период не попадает в перенос
                    if (strtotime($vp->DateEnd) < strtotime($this->vacationMove->DateStartWork) || 
                        strtotime($vp->DateStart) > strtotime($this->vacationMove->DateEndWork)) continue;
                    //период попадает в перенос
                    $vp->Moved = 1;
                    
                    $newWorkStart = $this->vacationMove->DateStartWork;
                    $newWorkEnd = $this->vacationMove->DateEndWork;
                    
                    if (strtotime($vp->DateStart) >= strtotime($this->vacationMove->DateStartWork))
                        $newWorkStart = $vp->DateStart;
                    if (strtotime($vp->DateEnd) <= strtotime($this->vacationMove->DateEndWork))
                        $newWorkEnd = $vp->DateEnd;
                    
                    $vp->FactDays = $vp->VacationDays - Vacation::calcDays(date('d.m.Y', strtotime($newWorkStart)), date('d.m.Y', strtotime($newWorkEnd)), $this->vacationMove->vacation->VacationTypeID);
             
                    $vp->save();   */ 
                }//}
            }
                
            Employee::updateAll(['IsActual' => 0], ['ID' => $rawID]);
            return true;
        }
        return false;
    }
    
    public function deleteOrder()
    {
        $needTransaction = Yii::$app->db->transaction;
        if(empty($needTransaction))
        $transaction = Yii::$app->db->beginTransaction();
            try {  
                if (!$this->isClosed() && isset($this->employee) && $this->employee->IsRaw == 1){
                    if ($this->employee->passportProcess && $this->EmployeeOrderTypeID == 7)
                        $this->employee->passportProcess->delete();
                    if ($this->vacation && $this->EmployeeOrderTypeID == 5){
                        foreach ($this->vacation->vacationPeriods as $vp) $vp->delete();
                        $this->vacation->delete();}
                    if ($this->vacationMove && in_array($this->EmployeeOrderTypeID, [9,10,12]))
                        $this->vacationMove->delete();
                    if ($this->vacationPlan && in_array($this->EmployeeOrderTypeID, [6,12])) 
                        $this->vacationPlan->delete();    
                    if ($this->attestation && in_array($this->EmployeeOrderTypeID, [8])) 
                        $this->attestation->delete();    
                    if ($this->employeeHarmLinks){
                        foreach ($this->employeeHarmLinks as $ehl) $ehl->delete();}  
            $this->employee->delete();
            parent::delete();
        }
                if(empty($needTransaction)) {
                    $transaction->commit();
                    }
                return true;
            }
            catch(Exception $e) 
            {
                if(empty($needTransaction))
                    $transaction->rollback();
                throw $e;
                return false;
            }
        
    }
    
    public function getVacation() 
    {
	return $this->hasOne(Vacation::className(), ['EmployeeOrderID' => 'ID']);
    }
    
    public function getVacationMove() 
    {
	return $this->hasOne(VacationMove::className(), ['EmployeeOrderID' => 'ID']);
    }
    
    public function getAttestation() 
    {
	return $this->hasOne(Attestation::className(), ['EmployeeOrderID' => 'ID']);
    }
    
    public function getFinanceTypeName()
    {
        return $this->employeeAll && $this->employeeAll->staffPositionAll ? $this->employeeAll->staffPositionAll->financeTypeShortName : '';
    }
    
    public function getFinanceType()
    {
        return $this->employeeAll && $this->employeeAll->staffPositionAll ? $this->employeeAll->staffPositionAll->financeType : null;
    }
    
    public function getVacationPlan() 
    {
	return $this->hasOne(VacationPlan::className(), ['EmployeeOrderID' => 'ID']);
    }
    
    public function getStaffCategoryName()
    {
        return $this->employeeAll && $this->employeeAll->staffPositionAll ? $this->employeeAll->staffPositionAll->staffCategoryShortName : '';
    }
    
    public function getStaffCategory()
    {
        return $this->employeeAll && $this->employeeAll->staffPositionAll ? $this->employeeAll->staffPositionAll->staffCategory : '';
    }
    
    public function getMainDepartmentName()
    {
        if ($this->employeeAll && $this->employeeAll->staffPosition){
            $str = str_replace('100001::','',$this->employeeAll->staffPosition->department->TreeDepartment);            
            $d = Department::find()->andWhere(['ParentID' => substr($str,0,6)])->one();
            if (!is_null($d)) return $d->Name;
        }
        
        return  '';
    }
    
    public function getMainDepartment()
    {
        if ($this->employeeAll && $this->employeeAll->staffPosition){
            $str = str_replace('100001::','',$this->employeeAll->staffPosition->department->TreeDepartment);            
            $d = Department::find()->andWhere(['ParentID' => substr($str,0,6)])->one();
            if (!is_null($d)) return $d;
        }
        
        return  '';
    }
	
    public function getUserName()
    {
        return $this->employeeAll ? $this->employeeAll->userName : '';
    }
    
    public function numberGenerate()
    {
        if (in_array($this->EmployeeOrderTypeID,[2,4])){             
            $eoNumber = EmployeeOrder::find()->joinWith(['employee'])->andWhere(['ParentID' => $this->employee->ParentID, 'EmployeeOrderTypeID' => [2,4]])->max('Number');
            if (empty($eoNumber)) 
                $eoNumber = 1; 
            else $eoNumber++;
            
            return $eoNumber;
        }
        return null;
    }
}
