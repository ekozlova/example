<?php

namespace common\modules\ok\models;

use Yii;
use common\modules\persons\models\Person;
use common\modules\roles\models\AisUser;
use common\modules\plan\models\Vacancy;
use common\modules\persons\models\Passport;
use common\modules\persons\models\PassportProcess;
use common\components\Helper;
use common\modules\persons\models\PtLink;
use common\modules\ok\Module;
use yii\web\ServerErrorHttpException;
/**
 * This is the model class for table "{{%employee_new}}".
 *
 * @property integer $ID
 * @property double $Value
 * @property integer $StaffPositionParentID
 * @property integer $IsMain
 * @property string $StartDate
 * @property string $EndDate
 * @property integer $PersonID
 * @property integer $JobContractTypeID
 * @property integer $IsActual
 * @property integer $ParentID
 * @property string $VersionDate
 * @property string $DeletedDate
 * @property string $ContractDate
 * @property integer $EmployeeStateID
 * @property integer $UserID
 * @property integer $EmployeeOrderID
 * @property string $WorkDay
 * @property string $WorkHour
 * @property integer $VacationDay
 * @property integer $VacationDayDop
 * @property string $ProbationPeriod
 * @property string $Other
 * 
 * @property EmployeeState $employeeState
 * @property User $user
 * @property Person $person
 * @property StaffPosition $staffPosition
 * @property JobContractType $jobContractType
 * @property Vacancy $vacancy
 */
class Employee extends \common\components\VersionedActiveRecord
{
	public $val;
        public $ok = false;
        public $Person;
        public $DepartmentParentID;       
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%employee_new}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Value', 'IsMain', 'JobContractTypeID', 'EmployeeStateID', 'JobTypeID', 'StartDate', 'Harm'], 'required'],
            [['StaffPositionParentID'], 'required', 'when' => function($model){return true;}, 
                            'whenClient'=>'function (attribute, value) {return $("#employeeorder-employeeordertypeid").length==0 || $("#employeeorder-employeeordertypeid").val()==2}'],
            [['EndDate'], 'required', 'when' => function ($model) {
                return $model->JobContractTypeID == 1 && empty($model->Other);
                }, 'whenClient' => "function (attribute, value) { return false;
                   //return $('#employee-jobcontracttypeid').val()==1 && $('#employee-other').val() == '';
            }", 'message' => 'Для срочного договора заполните дату конца или укажите основание в примечании'],  
           // [['EndDate'],'validateEndDate'],        
            [['Value'], 'number', 'min' => '0.001', 'max' => '1'],//fail - cleaners could have 2 in sum!!!
            [['VacationCompensation'], 'number', 'min' => '-150', 'max' => '600'],
            [['StaffPositionParentID', 'IsMain', 'PersonID', 'JobContractTypeID', 'ParentID', 'EmployeeStateID', 'UserID', 'EmployeeOrderID', 'DepartmentParentID','VacationID','JobTypeID', 'Harm', 'StaffReasonID', 'DecisionTypeID'], 'integer'],
            [['StartDate', 'EndDate', 'ContractDate', 'ChangeDate', 'DocumentDate', 'ProtocolDate'], 'safe'],
            [['WorkDay', 'WorkHour', 'ProbationPeriod', 'Other', 'ProtocolNumber', 'ContractNumber'], 'string'],
            [['VacationDay', 'VacationDayDop'], 'integer', 'min' => 0],
            ['Person', 'required', 'when' => function ($model) {
                return $model->ok;
                }, 'whenClient' => "function (attribute, value) {
                   return $('#employee-person').length > 0;
            }"],
            ['Person',  'common\components\validators\autoCompleteField', 'required'=>false],
           [['Value', 'IsMain'], 'validateEmployeeValue'], 
            [['IsMain'], 'validateIsMain'],
            [['Value'], 'validateIsNotMain'],            
            [['Value'], 'validateSPValue'],    
            [['Harm'], 'validateHarm'],
            [['Value'], 'validateVacationValue'],
            [['PersonID'], 'validateEmployeeState'],
            [['ChangeDate'], 'validateChangeDate', 'skipOnEmpty' => false],
           // [['StartDate', 'EndDate', 'ContractDate', 'ChangeDate', 'DocumentDate', 'ProtocolDate'], 'date', 'min' => '01.01.2000', 'max' => '31.12.2100']
       ];
    }
   /* 
    public function validateEndDate($attribute, $params)
    {
        if ($this->JobContractTypeID == 1 && empty($model->Other)){
            $this->addError($attribute, 'Для срочного договора заполните дату конца или укажите основание в примечании');
        }
    }*/
    
    public static function versionColumn($columns = []){
            return parent::versionColumn(['subKeyColumn' => 'IsRaw']);
        }

    public function validateEmployeeValue($attribute, $params)
    {
        if ($this->IsMain == 1 && ($this->Value > 1 || $this->Value <= 0)){
            $this->addError($attribute, 'Неверное значение кол-во: должно быть больше 0 и не больше 1');
        } elseif ($this->IsMain < 1 && ($this->Value > 0.5 || $this->Value <= 0)){
            $this->addError($attribute, 'Неверное значение кол-во: должно быть больше 0 и не больше 0.5');
        }
    }
    
    public function validateIsMain($attribute, $params)
    {
        if ($this->IsMain == 1 && $this->employeeOrder && in_array($this->employeeOrder->EmployeeOrderTypeID , [1,2,4])){
            $e = Employee::find()->andWhere(['PersonID' => $this->PersonID, 'IsRaw' => 0, 'IsActual' => 1, 'IsMain' => 1])
                    ->andWhere($this->ParentID > 0 ? 'ParentID <> '.$this->ParentID : '')->andWhere('EmployeeStateID <> 100390')->one();
            if (!is_null($e))
                $this->addError($attribute, 'Данное физ. лицо уже работает на основную ставку');
        } 
    }
    
    public function validateIsNotMain($attribute, $params)
    {
        if ($this->IsMain < 1 && $this->employeeOrder && in_array($this->employeeOrder->EmployeeOrderTypeID , [1,2,4])){
            $es = Employee::find()->andWhere(['PersonID' => $this->PersonID, 'IsRaw' => 0, 'IsActual' => 1])
                    ->andWhere($this->ParentID > 0 ? 'ParentID <> '.$this->ParentID : '')->andWhere('IsMain < 1')->andWhere('EmployeeStateID <> 100390')->all();
            $sum = 0;
            foreach ($es as $e){
                $sum += $e->Value; 
            }
            
            if (round($sum + $this->Value - 0.5, 3) > 0)
                $this->addError($attribute, 'Данное физ. лицо уже работает по совместительству на '.$sum.', суммарное кол-во ставок превысит 0.5');
        } 
    }
    
    public function validateSPValue($attribute, $params)
    {
        if ($this->staffPosition && $this->employeeOrder && in_array($this->employeeOrder->EmployeeOrderTypeID , [1,2/*,4*/])){
            if (round($this->Value - $this->staffPosition->Value,3) > 0){
                $this->addError($attribute, 'Заданное количество ставок превышает допустимое ('.$this->staffPosition->Value.').');
            }
            $es = Employee::find()->andWhere(['StaffPositionParentID' => $this->StaffPositionParentID, 'IsRaw' => 0, 'IsActual' => 1, 'EmployeeStateID' => 100388])
                    ->andWhere($this->ParentID > 0 ? 'ParentID <> '.$this->ParentID : '')->all();  
            $sum = 0;
            foreach ($es as $e){              
                $sum += $e->Value; 
            }  
            $em = Employee::find()->andWhere(['ParentID' => $this->ParentID, 'IsRaw' => 0, 'IsActual' => 1])->andWhere('EmployeeOrderID <> '.$this->EmployeeOrderID)->one();
           
            if (round($sum + $this->Value - $this->staffPosition->Value,3) > 0 && (in_array($this->employeeOrder->EmployeeOrderTypeID, [1])|| round($em->Value - $this->Value,3) < 0)){
                $this->addError($attribute, 'Заданное количество ставок превышает свободное ('.($this->staffPosition->Value - $sum).').');
            }
        }
    }
    
    public function validateHarm($attribute, $params)
    {
        if ($this->staffPosition && $this->Harm){
             $e = Employee::find()->andWhere(['PersonID' => $this->PersonID, 'IsRaw' => 0, 'IsActual' => 1, 'Harm' => 1])
                    ->andWhere($this->ParentID > 0 ? 'ParentID <> '.$this->ParentID : '')->andWhere('EmployeeStateID <> 100390')->one();
            if (!is_null($e))
             $this->addError($attribute, 'Данное физ. лицо уже работает по вредности.');
        }
    }
    
    public function validateVacationValue($attribute, $params)
    {
        if ($this->staffPosition && $this->employeeOrder && in_array($this->employeeOrder->EmployeeOrderTypeID , [13])){
            if ($this->Value > 0.9){
                 $this->addError($attribute, 'Допустимо не более 0.9 ставки.');
            }
        }
    }
    
    public function validateEmployeeState($attribute, $params)
    {
        if ($this->staffPosition && $this->employeeOrder && in_array($this->employeeOrder->EmployeeOrderTypeID , [13, 14])){
            $ce = Employee::find()->andWhere(['ParentID' => $this->ParentID, 'IsRaw' => 0, 'IsActual' => 1, 'EmployeeStateID'  => 100391])->count();            
            if ($ce == 0){
                 $this->addError($attribute, 'Сотрудник должен иметь статус "Временно отсутствует".');
            }
        }
    }
    
    public function validateChangeDate($attribute, $params)
    {
        if ($this->staffPosition && $this->employeeOrder && in_array($this->employeeOrder->EmployeeOrderTypeID , [2,3,4,8,13,14])){                      
            if (empty($this->ChangeDate)){
                 $this->addError($attribute, 'Заполните поле "Дата изменения".');
            }
        }
    }
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => Module::t('ML', 'ID'),
            'Value' => Module::t('ML', 'Value'),
            'StaffPositionParentID' => Module::t('ML', 'Staff Position'),
            'IsMain' => Module::t('ML', 'Is Main'),
            'IsMainText' => Module::t('ML', 'Is Main'),
            'StartDate' => Module::t('ML', 'Date Start'),
            'EndDate' => Module::t('ML', 'Date End'),
            'PersonID' => Module::t('ML', 'Person'),
            'JobContractTypeID' => Module::t('ML', 'Job Contract Type'),           
            'ParentID' => Module::t('ML', 'Parent'),           
            'ContractDate' => Module::t('ML', 'Date Contract'),
            'EmployeeStateID' => Module::t('ML', 'Status'),
            'UserID' => Module::t('ML', 'User'),
            'EmployeeOrderID' => Module::t('ML', 'Employee Order'),
            'WorkDay' => Module::t('ML', 'Work Day'),
            'WorkHour' => Module::t('ML', 'Work Hour'),
            'VacationDay' => Module::t('ML', 'Vacation Day'),
            'VacationDayDop' => Module::t('ML', 'Vacation Day Dop'),
            'ProbationPeriod' => Module::t('ML', 'Probation Period'),
            'Other' => Module::t('ML', 'Other'),
            'DepartmentParentID' =>Module::t('ML', 'Department'),
            'VersionDate' => Module::t('ML', 'Version Date'),
            'status' => Module::t('ML', 'Status'),
            'ChangeDate' => Module::t('ML', 'Change Date'),
            'VacationID' => Module::t('ML', 'Vacation'),
            'JobTypeID' => Module::t('ML', 'Job Type'),
            'Harm' => Module::t('ML', 'Harm'),
            'StaffReasonID' => Module::t('ML', 'Staff Reason'),
            'DocumentDate' => Module::t('ML', 'Document Date'),
            'VacationCompensation' => Module::t('ML', 'Vacation Compensation'),
            'DecisionTypeID' => Module::t('ML', 'Decision Type'),
            'ProtocolNumber' => Module::t('ML', 'Protocol Number'),
            'ProtocolDate' => Module::t('ML', 'Protocol Date'),
            'ContractNumber' =>  Module::t('ML', 'Contract Number'),
        ];
    }
	
    
    public function afterSave($insert = true, $changedAttributes = array()) {
        parent::afterSave($insert, $changedAttributes);
        if( $this->IsRaw == 0 && $this->staffPositionAll->StaffCategoryID == 4){
            $StudYear = Date('Y')-(Date('m')<9 ? 1 : 0);
            try {
            EmployeePlanLoad::reload($StudYear, $this);}
            catch (\Exception $e) 
            {
                Yii::$app->session->setFlash('danger', Module::t('ML', '{modelClass} reload error', 
                        ['modelClass' => Module::t('ML', 'Employee'),]));
            }
        }
    }
	/** 
	 * @return \yii\db\ActiveQuery 
	 */ 
	public function getEmployeeState() { 
		return $this->hasOne(EmployeeState::className(), ['ID' => 'EmployeeStateID']);
	}
 
   /** 
    * @return \yii\db\ActiveQuery 
    */ 
   public function getJobContractType() {
		return $this->hasOne(JobContractType::className(), ['ID' => 'JobContractTypeID']);
   }
 
   /**
	 * @return \yii\db\ActiveQuery 
	 */
	public function getPerson() {
		return $this->hasOne(Person::className(), ['ID' => 'PersonID']);
	}

	/**
	 * @return \yii\db\ActiveQuery 
	 */
	public function getStaffPosition() {
		return $this->hasOne(StaffPosition::className(), ['ParentID' => 'StaffPositionParentID']);
	}
	
	public function getStaffPositionAll() {
		return $this->hasOne(StaffPositionAll::className(), ['ParentID' => 'StaffPositionParentID'])->where(StaffPosition::tableName().'.IsActual > -1');
	}

	/**
	 * @return \yii\db\ActiveQuery 
	 */
	public function getUser() {
		return $this->hasOne(AisUser::className(), ['ID' => 'UserID']);
	}

	public function getVacancy() {
		return $this->hasOne(Vacancy::className(), ['EmployeeParentID' => 'ParentID']);
	}
        
        public function getEmployeeHarmLinks() 
        {
            return $this->hasMany(EmployeeHarmLink::className(), ['EmployeeOrderID' => 'EmployeeOrderID']);
	}        

	public function insert($runValidation = true, $attributes = null) {
		$this->UserID = \Yii::$app->user->id;
                if ($this->JobContractTypeID == 2 && $this->employeeOrder && $this->employeeOrder->EmployeeOrderTypeID!=3) $this->EndDate = null;
                
                $this->EndDate = Helper::CheckDateTimeMatch($this->EndDate);
                $this->StartDate = Helper::CheckDateTimeMatch($this->StartDate);
                $this->ContractDate = Helper::CheckDateTimeMatch($this->ContractDate);
                $this->ChangeDate = Helper::CheckDateTimeMatch($this->ChangeDate);
                $this->DocumentDate = Helper::CheckDateTimeMatch($this->DocumentDate);
                $this->ProtocolDate = Helper::CheckDateTimeMatch($this->ProtocolDate);
                
                //Helper::CheckModelDates($this,['EndDate', 'StartDate', 'ContractDate', 'ChangeDate']);                
		return parent::insert($runValidation, $attributes);
	}

	public function getName() {
		return "{$this->PersonHRwoBD} / {$this->StaffPositionName} / {$this->Value} ст";
	}
	
	public function getStaffPositionName() {
        static $name;
        if(!isset($name[$this->StaffPositionParentID])){
            $v=$this->staffPosition;
            $name[$this->StaffPositionParentID] = ($v = $this->staffPosition)? $v->Name : null;;
        }
        return $name[$this->StaffPositionParentID];
	}

	public function getEmployeeStateName() {
        static $name;
        if(!isset($name[$this->EmployeeStateID])){
            $name[$this->EmployeeStateID] = empty($this->employeeState)? null :$this->employeeState->Name;
        }
        return $name[$this->EmployeeStateID];
	}

	public function getJobContractTypeName() {
        static $name;
        if(!isset($name[$this->JobContractTypeID])){
            $name[$this->JobContractTypeID] = empty($this->jobContractType)? null : $this->jobContractType->Name;
        }
        return $name[$this->JobContractTypeID];
	}
        
        public function getJobTypeName() {
        static $name;
        if(!isset($name[$this->JobTypeID])){
            $name[$this->JobTypeID] = empty($this->jobType)? null : $this->jobType->Name;
        }
        return $name[$this->JobTypeID];
	}

	public function getUserName() {
		return $this->user->username;
	}
	
	public function getPersonHRwoBD() {
		return ($p = $this->passport)? $p->PersonHRwoBD : null;
	}
        
        public function getPersonHR() {
            $p = $this->person;
            return isset($p)? $p->personHR : null;
	}
    
        public function getInitialAttribute() {
            return $this->personHR;
        }
        
        /**
	 * @return \yii\db\ActiveQuery 
	 */
	public function getEmployeeOrder() {
		return $this->hasOne(EmployeeOrder::className(), ['ID' => 'EmployeeOrderID']);
	}
        
	public static function getAllEmployeeOrders($parentID = 0) 
        {
		return Employee::find()->joinWith(['employeeOrder'])->where('IsActual = 1 OR IsRaw = 0')
                        ->andWhere(['ParentID' => $parentID])->orderBy('ID DESC')->all();
	}
        
        /*public static function getPositiveValue($personID, $except = 0)
        {
            if ($except > 0)
                $cond = 'ParentID <> '.$except;
            else $cond = '';
            $personEmpls = self::find()->andWhere(['PersonID' => $personID])->andWhere($cond)->all();
            $sum = 0;
            foreach ($personEmpls as $empl){
                $sum += $empl->Value;
            }
            return $sum;
        }*/
        
        public function getPassport() 
        {
            return $this->hasOne(Passport::className(), ['PersonID' => 'PersonID']);
	}
        
        public function getPassportProcess() 
        {
            return $this->hasOne(PassportProcess::className(), ['PersonID' => 'PersonID']);
	}
        
        public function getDepartmentName()
        {
            return $this->staffPosition ? $this->staffPosition->departmentName : '';
        }
        
        public function getIsMainText()
        {
            return $this->IsMain > 0 ? Module::t('ML', 'Main') : ($this->IsMain < 0 ? Module::t('ML', 'Outer') : Module::t('ML', 'Inner'));
        }
        
        public function getCurrentStaffPosition()
        {
            $sp = new StaffPosition;
            
            if (!empty($this->ParentID) && ($ep = Employee::find()->andWhere(['ParentID' => $this->ParentID, 'IsRaw' => 0])->one()) && !empty($ep->StaffPositionParentID)){
                $sp->attributes = $ep->staffPosition->attributes;
            }
            else if (!empty($this->StaffPositionParentID)){
                $sp->attributes = $this->staffPosition->attributes;                
            }
            return $sp;
        }
        
        public function getStatus()
        {
            return $this->IsActual == 0 ? 'Не актуальный' : ($this->IsRaw == 1 ? 'В процессе' : 'Актуальный');
        }
        
        public function getVacation() 
        {
            return $this->hasOne(Vacation::className(), ['EmployeeOrderID' => 'EmployeeOrderID']);
	}
        
        public function getJobType() 
        {
            return $this->hasOne(JobType::className(), ['ID' => 'JobTypeID']);
        }
        
        public function calcVacationPeriods($dateStart, $dateEnd, $vacationtypeid, $days = 0)
        {
            //all vacations by employee
            $vpname = VacationPeriod::tableName();
            
            $vacps = VacationPeriod::find()->joinWith('vacation.employeeAll')->andWhere(['ParentID' => $this->ParentID, 'IsRaw' => 0])
                    ->andWhere($vpname.'.Moved = 1 AND '.$vpname.'.FactDays < VacationDays')->andWhere(['VacationTypeID' => $vacationtypeid])->orderBy('PeriodDateStart ASC')->one();
            if (is_null($vacps)){
                $vacps = VacationPeriod::find()->joinWith('vacation.employeeAll')->andWhere(['ParentID' => $this->ParentID, 'IsRaw' => 0])
                    ->andWhere($vpname.'.Moved = 0')->andWhere(['VacationTypeID' => $vacationtypeid])->orderBy('PeriodDateStart DESC')->one();
            }
            if ($days > 0){
                $dateEnd = self::getEndPeriodDate($dateStart, $days, $vacationtypeid);
            }
            else {
                $days = Vacation::calcDays($dateStart, $dateEnd, $vacationtypeid);
            }
            $factDays = 0; $lastperiod = []; $result = [];
           
            switch ($vacationtypeid){
                case 1: $vacationDays = $this->VacationDay; break;
                case 2: $vacationDays = $this->VacationDayDop; break;
                default : return [0 => ['days' => $days]];   
            }
            if (empty($vacationDays)) return [0 => ['days' => $days]];   
            if (is_null($vacps)){ // нет ни одного отпуска
                $lastperiod = self::getPeriodByDateStart($this->StartDate);//период - год с начала работы
                $factDays = 0;//отгуляно дней                 
            } else {
                $lastperiod = [0 => self::dateCheck($vacps->PeriodDateStart)->format('d.m.Y'), 1 => self::dateCheck($vacps->PeriodDateEnd)->format('d.m.Y')];//период - последний период, в котором были фактически отгулянные дни
                $factDays = Vacation::factDaysInPeriod($this->ParentID, $vacationtypeid, $lastperiod[0]);
                /*
                if ($vacps->vacation->Moved > 0){
                    $factDays = $vacps->vacation->FactDays;//отгуляно дней факт. после переносов/отзывов/отмен
                }
                else {
                    $factDays = VacationPeriod::find()->andWhere(['VacationID' => $vacps->VacationID])->sum('VacationDays'); //отгуляно дней факт. без переносов/отзывов/отмен
                }*/
                if ($vacationDays <= $factDays){ //в последнем периоде все дни отгуляны
                    $lastperiod = self::getNextPeriod($lastperiod);
                    $factDays = 0;
                }
                
            }     
            //дней в первом периоде
            $daysFP = $days > $vacationDays - $factDays ? $vacationDays - $factDays : $days;
            
            $result[] = self::getPeriodByDays($lastperiod, $daysFP, $dateStart, $vacationtypeid);//выделяем первый период
            
            $firstDateEnd = $result[0]['vacdates'][1];
            $leftDays = $days - $daysFP; //осталось дней
            
            while ($leftDays > 0){//при необходимости выделяем еще периоды
                $newperiod = self::getNextPeriod($lastperiod);
                $newDateStart = self::getEndPeriodDate($firstDateEnd, 2);
                $fd = Vacation::factDaysInPeriod($this->ParentID, $vacationtypeid, $newperiod[0]);
                $newDays = $leftDays > $vacationDays ? $vacationDays : $leftDays;
                if ($newDays > $vacationDays - $fd) $newDays = $vacationDays - $fd;
                $res = self::getPeriodByDays($newperiod, $newDays, $newDateStart, $vacationtypeid);
                $firstDateEnd = $res['vacdates'][1];
                $result[] = $res;
                $leftDays = $leftDays - $newDays;
                $lastperiod = $newperiod;
            }
            
           return $result;
        }
    
    public static function getNextPeriod($period)
    {
        $dateStart = date_add(self::dateCheck($period[0]), date_interval_create_from_date_string('1 year'));                           
        $dateEnd = date_add(self::dateCheck($period[1]), date_interval_create_from_date_string('1 year'));
        return [0 => $dateStart->format('d.m.Y'), 1 => $dateEnd->format('d.m.Y')];
    }
    
    public static function getPeriodByDateStart($dateStart)
    {
        $dateEnd = date_add(self::dateCheck($dateStart), date_interval_create_from_date_string('1 year')); 
        $dateEnd = date_sub(self::dateCheck($dateEnd), date_interval_create_from_date_string('1 day'));
        return [0 => self::dateCheck($dateStart)->format('d.m.Y'), 1 => $dateEnd->format('d.m.Y')]; 
    }
    
    public static function getEndPeriodDate($dateStart, $days, $vacationtypeid = 0)
    {
        $dateEnd = date_add(self::dateCheck($dateStart), date_interval_create_from_date_string(($days - 1).' days'));
        $hc = in_array($vacationtypeid, [1,2]) ? Holidays::CountInInterval($dateStart, $dateEnd->format('d.m.Y')) : 0;
        if ($hc > 0){
            $dateEnd = date_add(self::dateCheck($dateEnd), date_interval_create_from_date_string($hc.' days'));
        }
        return $dateEnd->format('d.m.Y');
    }
    
    public static function getPeriodByDays($period, $days, $dateStart, $vacationtypeid = 0)
    {
        return ['period' => $period, 'days' => $days, 'vacdates' => [0 => $dateStart, 1 => self::getEndPeriodDate($dateStart, $days, $vacationtypeid)]];
    }
    
    public static function dateCheck($date)
    {
        return (is_object($date) && get_class($date) == 'DateTime') ? $date : date_create_from_format('d.m.Y', date('d.m.Y', strtotime($date)));
    }
    
    public static function getVacationsToMove($parentID = 0)
    {        
        $vpname = VacationPeriod::tableName();
        $vacps = VacationPeriod::find()->joinWith('vacation.employeeAll')->andWhere(['ParentID' => $parentID, 'IsRaw' => 0])
                    ->andWhere($vpname.'.Moved = 1 AND '.$vpname.'.FactDays < VacationDays')->orderBy('PeriodDateStart ASC')->one();
        $v = (!is_null($vacps)) ? [$vacps->vacation] : [];
        
        return array_merge($v, Vacation::find()->joinWith('employeeAll')->andWhere(['ParentID' => $parentID, 'IsRaw' => 0, 'Moved' => 0])->orderBy('ID desc')->limit(3)->all());
    }       
    
    public function getStaffReason() 
    { 
	return $this->hasOne(StaffReason::className(), ['ID' => 'StaffReasonID']);
    }
    
    public function getVacationPlans()
    {
        return $this->hasMany(VacationPlan::className(), ['PersonID' => 'PersonID']);
    }
    
    public function setPersonCode()
    {
        if (empty($this->person->PersonCodeID)){
            Yii::$app->db->createCommand('update tbl_person set PersonCodeID = (select MAX(PersonCodeID)+1 from tbl_person) WHERE ID ='.$this->PersonID)->execute();
        }
    }
    
    public function setPtlink(){
            $pt = new PtLink;
            $pt->PersonID = $this->PersonID;
            $pt->PersonTypeID = 2;
            $pt->DocumentNumber = $this->person->PersonCodeID;
            $pt->save();
    }
    
    public function getStaffPositionsWithSimilarPost()
    {
        if ($this->StaffPositionParentID > 0){
            $spos = StaffPosition::find()->joinWith(['job'])->andWhere([
                'PostID' => $this->staffPositionAll->job->PostID, 
                'DepartmentParentID' => $this->staffPositionAll->DepartmentParentID,
                'StaffCategoryID' => $this->staffPositionAll->StaffCategoryID])->all();
            
            return $spos;
        }
        return [];
    }
    
    public function getDecisionType() 
    {
        return $this->hasOne(DecisionType::className(), ['ID' => 'DecisionTypeID']);
    }
    
    public static function getProtocol($departmentParentID)
    {
        $sp = StaffPosition::tableName();
        $e = Employee::tableName();
        $result = [];
        $employees = Employee::find()->joinWith(['staffPosition','employeeState'],false)
                ->andWhere(['DepartmentParentID' => $departmentParentID, $sp.'.IsRaw' => 0, $e.'.IsRaw' => 0, 'Label' => 'valid'])              
                ->all();
        foreach ($employees as $employee){
            $result[$employee->ParentID]['ProtocolNumber'] = empty($employee->ProtocolNumber) ? '' : $employee->ProtocolNumber;
            $result[$employee->ParentID]['ProtocolDate'] = empty($employee->ProtocolDate) ? '' : date('Y-m-d', strtotime($employee->ProtocolDate));
            $result[$employee->ParentID]['DecisionTypeID'] = empty($employee->DecisionTypeID) ? '' : (string)$employee->DecisionTypeID;
        }
        
        return ['result' => $result];
    }
    
    public static function saveProtocol($attributes)
    {
        if (!isset($attributes['Employee']['ParentID'])) {throw new ServerErrorHttpException();} 
        $employeeParentID = $attributes['Employee']['ParentID'];
        unset($attributes['Employee']['ParentID']);
        $attributes['Employee']['ProtocolDate'] = date('d.m.Y', strtotime($attributes['Employee']['ProtocolDate']));
        if (isset($attributes['Employee']['oldprotocol'])) unset($attributes['Employee']['oldprotocol']);
        Employee::updateAll($attributes['Employee'], ['ParentID' => $employeeParentID, 'IsActual' => 1, 'IsRaw' => 0]);         
        return true;
    }
    
    public function contractNumberGeneration()
    {
        $year = is_null($this->ContractDate) ? date('y', strtotime($this->StartDate)) : date('y', strtotime($this->ContractDate));        
        $category = str_pad($this->staffPosition->StaffCategoryID,2,'0',STR_PAD_LEFT);
        
        $s = Yii::$app->db->createCommand("select max(ContractNumber)+1 from tbl_employee_new where ContractNumber like '{$year}{$category}%'")->queryScalar();
        if(!empty($s) && $s>0){
            return $s;
        }else{
            return $year.$category.'0001';
        }
    }
}
