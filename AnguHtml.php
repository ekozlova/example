<?php
namespace common\components;

use Yii;
use yii\helpers\Html;
use common\modules\department\assets\uiAnguCompleteAsset;
use common\assets\uiAnguYaTreeviewAsset;

class AnguHtml {
        
 public static function autoCompleteField($model, $attribute, $options = [], $view = null, $controller = null, $module = null, 
            $suggestMethod = 'suggest')
 {
    if (!is_null($view)) uiAnguCompleteAsset::register($view);
    $label = isset($options['labelName']) ? $options['labelName'] : self::labelName($model, $attribute);  
    
    if (is_null($module) && !is_null($view)) {$module = $view->context->module->id;}
    if (is_null($controller)) {$controller = strtolower(preg_replace('/(\[.*\])|(ID$)/i', '', $attribute));}
    
    $params = '';
    if (isset($options['remote-params']) && is_array($options['remote-params'])){
        foreach ($options['remote-params'] as $key => $param){
            $params .= "{$key}={$param}&";
        }
    }
    if(isset($options['div'])) {
        $divOptions = $options['div']; unset($options['div']);
    }
    else{
        $divOptions = [];
    }
    
    $clientOptions = array_merge([
        'id' => $attribute,
        'placeholder' => Yii::t('ML', 'Enter {attr}', ['attr' => $label]),
        'pause' => 400,
        'selected-object' => 'select'.preg_replace('/(\[.*\])|(ID$)/i', '', $attribute),
        'remote-url' => "/{$module}/{$controller}/{$suggestMethod}?{$params}term=",
        'remote-url-data-field' => 'results',     
        'title-field' => 'label', 
        'input-class' => 'form-control',   
        'initial-value' => '', 
        ], $options); 
    
    $output =  Html::beginTag('div', array_merge(['class' => 'form-group'], $divOptions));
    if ($label)
        $output .= self::label(null, null, ['labelName' => $label]);
    $output .= Html::tag('angucomplete-alt', '', $clientOptions);
    $output .= Html::endTag('div');
    
    return $output;
 }
 
 public static function dropDownList($model, $attribute, $options = [])
 {
    if(isset($options['div'])) {
        $divOptions = $options['div']; unset($options['div']);
    }
    else{
        $divOptions = [];
    } 
    $output = Html::beginTag('div', array_merge(['class' => 'form-group'], $divOptions));
    $output .= self::label($model, $attribute, isset ($options['labelName']) ? ['labelName' => $options['labelName']] : []);
    
    $ngoptions = $options['ng'];
    unset($options['ng']);
    if (is_string($ngoptions['repeat']) && strpos($ngoptions['repeat'], ' in ')===false){
        $ngoptions['repeat']=[$ngoptions['repeat']];
    }    
    if (is_array($ngoptions['repeat'])){
        $array = $ngoptions['repeat'][0];
        $item = isset($ngoptions['repeat'][1]) ? $ngoptions['repeat'][1] : 'item';
        $ngoptions['repeat'] = "{$item} in {$array}";
    }
    if (!isset($ngoptions['key'])) $ngoptions['key'] = 'ID';
    if (!isset($ngoptions['value'])) $ngoptions['value'] = 'Name';
    $key = (isset($item) ? $item.'.' : '').$ngoptions['key'];
    $value = (isset($item) ? $item.'.' : '').$ngoptions['value'];
    $clientOptOptions = isset($options['options']) ? $options['options'] : [];
    unset($options['options']);
       
    $clientOptions = array_merge([
        'class' => 'form-control',             
        /*'prompt' => Yii::t('ML', 'Select {modelClass}', ['modelClass' => $label]),*/
        'ng-model' => isset($ngoptions['model']) ? $ngoptions['model'] : '', 
        'ng-options' => "{$key} as {$value} for ".$ngoptions['repeat'],
        'options' => $clientOptOptions,
        ], $options); 
    $output .= Html::dropDownList('', '', [], $clientOptions);
    $output .= Html::endTag('div');
    
    return $output;
 }

 public static function label($model, $attribute, $options = [])
 {
     $label = isset($options['labelName']) ? $options['labelName'] : self::labelName($model, $attribute);
     $output = Html::label($label, null, array_merge(['class' => 'control-label'], $options));
     
     return $output;
 }
 
 public static function labelName($model, $attribute)
 {
      return (is_string($model)) ? (new $model)->attributeLabels()[$attribute] : $model->attributeLabels()[$attribute];
 }
   
 public static function textInput($model, $attribute, $options = [])
 {
    return self::text('textInput', $model, $attribute, $options);
 } 
 
 public static function textarea($model, $attribute, $options = [])
 {
    return self::text('textarea', $model, $attribute, $options);
 } 
 
 private static function text($type, $model, $attribute, $options = [])
 {
    if((isset($options['inputType']) && $options['inputType'] == 'date') || $type == 'dateInput') 
        return self::dateInput($model, $attribute, $options); 
    if(isset($options['div'])) {
        $divOptions = $options['div']; unset($options['div']);
    }
    else{
        $divOptions = [];
    } 
    if (!isset($options['name'])) $options['name'] = '';
    $label = isset($options['labelName']) ? $options['labelName'] : self::labelName($model, $attribute);    
    $output =  Html::beginTag('div', array_merge(['class' => 'form-group'], $divOptions));    
    if ($label)
        $output .= self::label(null, null, ['labelName' => $label]);    
    $output .= Html::$type($options['name'], '', array_merge(['class' => $type == 'checkbox' ? 'bigCheckBox' : 'form-control',], $options));
    $output .= Html::endTag('div');
    
    return $output;
 }
 
 public static function dateInput($model, $attribute, $options = [])
 {
    if(isset($options['div'])) {
        $divOptions = $options['div']; unset($options['div']);
    }
    else{
        $divOptions = [];
    } 
    if (!isset($options['name'])) $options['name'] = '';
    
    $label = isset($options['labelName']) ? $options['labelName'] : self::labelName($model, $attribute);
    $output =  Html::beginTag('div', array_merge(['class' => 'form-group'], $divOptions));
    if ($label)
        $output .= self::label(null, null, ['labelName' => $label]);    
    $output .= Html::input('date',$options['name'], '', array_merge(['class' => 'form-control',], $options));
    $output .= Html::endTag('div');
    
    return $output;
 }
 
 public static function checkbox($model, $attribute, $options = [])      
 {
    return self::text('checkbox', $model, $attribute, $options); 
 }
 
 public static function a($text, $options = [])
 {
     return Html::a($text, null, $options);
 }
 
 public static function aButton($text, $options = [])
 {
    if (isset($options['btnType'])) {
         $btnType = $options['btnType'];
         unset($options['btnType']);
     }
     else {
        $btnType =  'default';
     }
     @$options['class'].= 'btn btn-'.$btnType;
     return self::a($text, $options);
 }
 
 public static function tagInit($initArr, $encode = true, $tag = 'div')
 {
     $init = [];
     foreach ($initArr as $name => $source){
         if ($encode) $source = json_encode($source);
         $init[] = "{$name} = ".$source;
     }
     return Html::tag($tag, '', ['ng-init' => implode('; ',$init)]);
 }
 
 public static function yaTreeView($options = [], $spanOptions = [], $view = null, $nodeContent = '{{node.$model.label}}')
 {
    if (!is_null($view)) uiAnguYaTreeviewAsset::register($view);
    $output =  Html::beginTag('div', array_merge(['ya-treeview' => true, 'ya-id' => 'myTree', 'ya-model' => 'model', 
        'ya-options' => 'options', 'ya-context' => 'context'], $options));
    $output .= Html::tag('span', $nodeContent, $spanOptions);    
    $output .= Html::endTag('div');
    
    return $output;
 }
 
 public static function errorBlock($contentVar = 'error', $options = [])
 {
     return Html::tag('div', '{{'.$contentVar.'}}', array_merge(['class' => 'alert alert-danger ng-hide', 'ng-show' => $contentVar.'.length > 0'], $options));
 }
 
 public static function gridView($columns = [])
 {
     $output = Html::beginTag('table', ['class' => 'table table-striped table-bordered']);
     $output .= Html::beginTag('thead');
     $th = [];
     foreach ($columns as $column){
         if (isset($column['attribute']) && isset($column['model'])) {
            $output .= Html::tag('th',self::labelName($column['model'], $column['attribute']));}            
         else if (isset($column['label'])) {$output .= Html::tag('th',$column['label']);}  
         else {$output .= Html::tag('th','');}
     }
     
     $output .= Html::endTag('thead');
     $output .= Html::endTag('table');
     return $output;
 }
 
 public static function spanButton($spanClass, $url = null, $options = [], $actionID = 0, $controllerID = 0, $module = 0)
 {
     $output = '';
     if (isset($options['condition'])){
         $condition = $options['condition'];
         unset($options['condition']);
     } elseif ($actionID != 0) {
         $condition = ACLRole::accessControllerAction($actionID, $controllerID, $module); 
     } else 
         $condition = true;
     if ($condition) {
         $span = Html::tag('span', '', ['class' => $spanClass]);
         if (isset($options['title']) && !isset($options['aria-label'])) $options['aria-label'] = $options['title'];      
         $output = Html::a($span, $url, array_merge(['data-pjax' => '0'], $options));    
     }  
     return $output;
 }
} 
?>

                                    
                                    
                               